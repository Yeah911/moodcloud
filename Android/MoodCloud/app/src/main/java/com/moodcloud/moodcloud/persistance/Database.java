package com.moodcloud.moodcloud.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;
import java.util.Map;

/**
 * Created by Yehouda on 08/03/2016.
 */
public class Database extends SQLiteOpenHelper {


    public Database(Context context, String name, String create, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.name = name;
        this.create = create;
        this.drop = "DROP TABLE IF EXISTS " + name + " ;";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(drop);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(drop);
        onCreate(db);
    }

    private String create;
    private Map<String, String> elements;
    private String name;
    private String drop;
}
