package com.moodcloud.moodcloud.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.Request.UserRequests;

/**
 * Created by Yo911 on 10/04/2016.
 */
public class ForgotPassword extends AppCompatActivity {
    private UserRequests usrReq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        usrReq = new UserRequests( getApplicationContext() );
        final EditText emailField = (EditText) findViewById(R.id.email);
        Button forgot = (Button) findViewById(R.id.send);

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailField.toString();
                if (!email.isEmpty()) {
                    // TODO : dosomething
                    Intent i = new Intent(ForgotPassword.this, Login.class);
                    startActivity(i);
                } else {
                    // TODO : show message error
                }
            }
        });

    }
}