package com.moodcloud.moodcloud.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.Request.UserRequests;

/**
 * Created by Yo911 on 10/04/2016.
 */
public class SignIn extends AppCompatActivity {
    private UserRequests usrReq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        usrReq = new UserRequests( getApplicationContext() );
        final EditText loginField = (EditText) findViewById(R.id.login);
        final EditText emailField = (EditText) findViewById(R.id.email);
        final EditText pwdField = (EditText) findViewById(R.id.pwd);
        final EditText confirmPwdField = (EditText) findViewById(R.id.confirm_pwd);
        Button signin = (Button) findViewById(R.id.singin);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginField.toString();
                String email = emailField.toString();
                String pwd = pwdField.toString();
                String confirmPwd = confirmPwdField.toString();
                if( pwd.isEmpty() || !pwd.equals(confirmPwd) ) {
                    // TODO : show message error
                    return;
                }
                if( !login.isEmpty() && !email.isEmpty() ) {
                    usrReq.register( new User(login, pwd, email) );
                    Intent i = new Intent(SignIn.this, Login.class);
                    startActivity(i);
                } else {
                    // TODO : show message error
                }
            }
        });

    }
}
