package com.moodcloud.moodcloud.model;

import java.util.Date;

/**
 * Created by Yehouda on 09/03/2016.
 */
public class Mood {

    public Mood() {}

    public Mood(int mark) {
        this.mark = mark;
    }

    public Mood(int mark, String comment) {
        this.mark = mark;
        this.comment = comment;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private int mark;
    private String comment;
    private Date date;
}
