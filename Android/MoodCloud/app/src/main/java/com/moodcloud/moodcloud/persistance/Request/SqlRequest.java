package com.moodcloud.moodcloud.persistance.Request;

/**
 * Created by Yehouda on 09/03/2016.
 */
public class SqlRequest {

    // tables creations
    public static final String create_user_tables = "CREATE TABLE User (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT, email TEXT);";
    public static final String create_history_tables = "CREATE TABLE history (id INTEGER PRIMARY KEY AUTOINCREMENT, action TEXT, login TEXT, date DATETIME, auto_login BOOLEAN);";
    public static final String create_mood_tables = "CREATE TABLE mood (id INTEGER PRIMARY KEY AUTOINCREMENT, id_user INTEGER FOREIGN KEY, mark INTEGER, comment TEXT, date DATETIME);";

    // selections
    public static  final String select_user = "select * from User Where id = ?";
    public static  final String select_existing_user = "select * from User Where login = ? AND password = ?";
    public static  final String select_mood = "select * mood Where id = ?";
    public static  final String select_all_mood = "select * mood Where id_user = ?";
}
