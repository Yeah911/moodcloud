package com.moodcloud.moodcloud.persistance.Request;

import android.content.ContentValues;
import android.content.Context;

import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.util.Utils;

import java.sql.Date;

/**
 * Created by Yehouda on 09/03/2016.
 */
public class HistoryRequests {

    public HistoryRequests(Context context) {
        access = new DAOAccess(context, name_table, SqlRequest.create_history_tables);
    }

    public void add(User usr, String action) {
        access.open();
        ContentValues cv = new ContentValues();
        cv.put("action", action);
        cv.put("login", usr.getLogin());
        cv.put("date", Utils.getCurrentDate());
        cv.put("auto_login", false);
        access.add(name_table, cv);
        access.close();
    }

    private DAOAccess access;
    private final String name_table = "history";
}
