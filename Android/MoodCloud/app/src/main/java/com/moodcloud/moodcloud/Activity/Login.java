package com.moodcloud.moodcloud.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.Request.UserRequests;

public class Login extends AppCompatActivity {

    private UserRequests usrRequest;
    private User usr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usrRequest = new UserRequests( getApplicationContext() );
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final EditText login = (EditText) findViewById(R.id.loginName);
        final EditText password = (EditText) findViewById(R.id.password);
        Button connect = (Button) findViewById(R.id.button2);
        Button inscription = (Button) findViewById(R.id.signinButton);
        Button forgotPwd = (Button) findViewById(R.id.forgetButton);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usr = usrRequest.getUser(login.toString(), password.toString());
                if( usr != null ) {
                    Intent i = new Intent(); //Login.this, Other.class);
                    //startActivity(i);
                } else {
                    // TODO : show message error
                }
            }

        });

        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, SignIn.class);
                startActivity(i);
            }
        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(); //Login.this, ForgotPassword.class);
                //startActivity(i);
            }
        });
    }
}
