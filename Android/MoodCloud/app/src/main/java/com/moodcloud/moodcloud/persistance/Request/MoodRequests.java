package com.moodcloud.moodcloud.persistance.Request;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.moodcloud.moodcloud.model.Mood;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.util.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yehouda on 09/03/2016.
 */
public class MoodRequests {

    public MoodRequests(Context context, User usr) {
        access = new DAOAccess(context, name_table, SqlRequest.create_mood_tables);
        this.user = usr;
    }

    public void add(Mood mood) {
        access.open();
        ContentValues cv = new ContentValues();
        cv.put("id_user", user.getId());
        cv.put("mark", mood.getMark());
        cv.put("comment", mood.getComment());
        cv.put("date", Utils.getCurrentDate());
        access.add(name_table, cv);
        access.close();
    }

    public Mood getMood(String id) {
        Mood mood = new Mood();
        Cursor cursor = access.get(SqlRequest.select_mood, new String[] {id});
        cursor.moveToNext();
        return setMood(cursor);
    }

    public List<Mood> getMoods() {
        List<Mood> moods = new ArrayList<>();
        Cursor cursor = access.get(SqlRequest.select_all_mood, new String[] {user.getId()});
        while ( cursor.moveToNext() ) {
            moods.add(setMood(cursor));
        }
        return moods;
    }

    private Mood setMood(Cursor cursor) {
        Mood mood = new Mood();
        mood.setMark(cursor.getInt(2));
        mood.setComment(cursor.getString(3));
        Date date = new Date(cursor.getString(4));
        mood.setDate(date);
        return mood;
    }

    private DAOAccess access;
    private User user;
    private final String name_table = "mood";

}
