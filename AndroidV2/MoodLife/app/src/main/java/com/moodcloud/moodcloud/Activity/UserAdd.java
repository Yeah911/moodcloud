package com.moodcloud.moodcloud.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserAdd extends AppCompatActivity implements View.OnClickListener{

    private List<String> elements = new ArrayList<>();
    private List<User> users = new ArrayList<>();
    private ListView list;
    private ArrayAdapter<String> adapter;
    private EditText pseudo;
    private View loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add);
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        loading = findViewById(R.id.loadingPanel);
        pseudo = (EditText) findViewById(R.id.editeTextUserSearch);

        list = (ListView) findViewById(R.id.listViewUserSearch);
        adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, elements);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                final int pos = position;
                new AlertDialog.Builder(UserAdd.this)
                        .setMessage("add friend ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                addFriend(pos);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

        Button btn = (Button) findViewById(R.id.buttonRechercher);
        btn.setOnClickListener(this);
    }

    private void addFriend(int position) {
//        requestFriend
        RequestParams rp = new RequestParams();
        rp.put("friendID", users.get(position).getId());
        AsyncResponse.sendRequestGet(Connection.URLRedirection.requestFriend, rp, new CallbackResponse(this) {
            @Override
            public void onSuccess() {
                Utils.toast(context, "demande envoyé");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch( v.getId() ) {
            case R.id.buttonRechercher :
                elements.clear();
                loading.setVisibility(View.VISIBLE);
                RequestParams params = new RequestParams();
                params.put("term", pseudo.getText().toString());
                searchUser(params);
                break;
        }
    }

    private void searchUser(RequestParams params) {
        AsyncResponse.sendRequestGet(Connection.URLRedirection.searchUser, params, new CallbackResponse(this) {
            @Override
            public void onSuccess(JSONArray json) {
                users.clear();
                for( int i = 0; i < json.length(); i ++ ) {
                    try {
                        JSONObject obj = json.getJSONObject(i);
                        User u = new User();
                        u.setId(obj.getString("value"));
                        u.setLogin(obj.getString("label"));
                        users.add(u);
                    } catch (JSONException e) {
                    }
                }
                if( users.isEmpty() ) {
                    Utils.toast(context, "aucun utilisateur n'utilise ce pseudo");
                    loading.setVisibility(View.GONE);
                    return ;
                }
                for(  User u : users ) {
                    if( !u.equals(Informations.user) )
                        elements.add(u.getLogin());
                }
                adapter.notifyDataSetChanged();
                loading.setVisibility(View.GONE);
            }
        });
    }
}
