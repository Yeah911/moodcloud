package com.moodcloud.moodcloud.Activity;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
/**
 * Created by 626 on 25/06/2016.
 */
public class ChatMessage {

    public String body, sender, receiver, senderName;
    public Date date;
    public long dateLong;
    public String Time;
    public String msgid;
    public boolean isMine;// Did I send the message.

    public ChatMessage() {
    }

    public ChatMessage(String Sender, String Receiver, String messageString,
                       String ID, boolean isMINE) {
        body = messageString;
        isMine = isMINE;
        sender = Sender;
        msgid = ID;
        receiver = Receiver;
        senderName = sender;
    }

}
