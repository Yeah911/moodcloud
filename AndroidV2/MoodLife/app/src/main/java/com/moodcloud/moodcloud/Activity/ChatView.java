package com.moodcloud.moodcloud.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ListView;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.applications.PubnubBroadcast;
import com.moodcloud.moodcloud.model.Message;
import com.moodcloud.moodcloud.model.MoodMessage;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.ChatAdapter;
import com.moodcloud.moodcloud.util.Utils;
import com.pubnub.api.PubnubException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;

import org.json.JSONArray;
import org.json.JSONObject;

public class ChatView extends AppCompatActivity implements OnClickListener{

    private EditText msg_edittext;
    private String user1 = "khushi", user2 = "khushi1";
    private Random random;
    ChatMessage chatMessage;
    ChatMessage chatMessage2;
    public ArrayList<ChatMessage> chatlist;
    public ChatAdapter chatAdapter;
    private String conversationId;
    private Message message;
    private String channel;
    ListView msgListView;
    private static boolean start;
    private boolean notif;

    private String convId= "conversationId";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chat_layout);
        start = true;
        notif = false;
        Intent intent = getIntent();
        String key = "message";
        chatlist = new ArrayList<ChatMessage>();
        chatAdapter = new ChatAdapter(this, chatlist);
        chatAdapter.clearList();
        if( intent.hasExtra(convId) ) {
            conversationId = intent.getStringExtra(convId);
            getMessages(intent.getStringExtra(convId));
        }


        if( intent.hasExtra(key) && !intent.hasExtra(convId)  ) {
            initChat( (Message) intent.getSerializableExtra(key ));
        }

        if( intent.hasExtra("origin") ){
            if( intent.hasExtra(key) && intent.hasExtra(convId)  )
                message = (Message) intent.getSerializableExtra(key );

            boolean mode = false;
            mode = intent.getBooleanExtra("origin",false);
                if(mode)
                    setTitle( " Discussion avec :  " + message.getTo().getLogin());
                else
                    setTitle( " Discussion avec :  " + message.getFrom().getLogin());
        }

        chatAdapter.notifyDataSetChanged();

        random = new Random();

        msg_edittext = (EditText) findViewById(R.id.messageEditText);
        msgListView = (ListView) findViewById(R.id.msgListView);
        ImageButton sendButton = (ImageButton) findViewById(R.id.sendMessageButton);
        sendButton.setOnClickListener(this);

        // ----Set autoscroll of listview when a new message arrives----//
        msgListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        msgListView.setStackFromBottom(true);
        msgListView.setAdapter(chatAdapter);

        try {
            getMessage(this.message.getConversation().getId());
        } catch (PubnubException e) {
            e.printStackTrace();
        }

    }

    public void sendTextMessage(View v) {
        String message = msg_edittext.getEditableText().toString();
        if (!message.equalsIgnoreCase("")) {
            chatMessage = new ChatMessage();
            chatMessage.isMine = true;
            chatMessage.body = message;
            chatMessage.date =  Utils.getDate( Utils.getCurrentDate() );
            chatMessage.Time = Utils.getCurrentTime();
            msg_edittext.setText("");
            sendMessage(message);
            
            chatAdapter.notifyDataSetChanged();
        }
    }



    private void initChat(Message message) {
        this.message = message;
        conversationId = message.getConversation().getId();
        retreiveConversation(conversationId);
    }

    private void sendMessage(final String mess) {
        if( message == null ) return;

        RequestParams rp = new RequestParams();
        rp.put("content", mess);
        rp.put(convId, this.message.getConversation().getId());
//        rp.put("dateTime", Utils.getCurrentDateUTC() + "" );
        rp.put("dateTime", "(new Date(" + Utils.getCurrentDateUTC() + "))" );
        AsyncResponse.sendRequestGet(Connection.URLRedirection.sendMessage, rp, new CallbackResponse() {
        });
    }

    private String format(String chan) {
        StringBuilder sb = new StringBuilder(chan);
        sb.insert(8, '-');
        sb.insert(13, '-');
        sb.insert(18, '-');
        sb.insert(23, '-');
        return sb.toString();
    }

    private void retreiveConversation(String convId) {
        RequestParams rp = new RequestParams();
        rp.put("conversationId", convId);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.retreiveConversation, rp, new CallbackResponse() {
            @Override
            public void onSuccess(JSONObject json) {
                message = Parser.parseMessagesConversation(json);
                List<MoodMessage> mm = message.getMessages();
                User receiver = message.getTo();
                User sender   = message.getFrom();
                String myEmail = Informations.user.getEmail();
                for(MoodMessage m : mm) {
                    ChatMessage cm = new ChatMessage(
                            sender.getLogin(),
                            receiver.getLogin(),
                            m.getContent(),
                            m.getId(),
                            m.getAuthor().getEmail().equals( myEmail.trim() )
                    );
                    cm.date = m.getDateCreate();
                    cm.dateLong = m.getDateCreateLong();
                    chatAdapter.add(cm);
                }
                Collections.sort(chatlist, getComparator());
                chatAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getMessage(String conversationId) throws PubnubException {
        channel = format(conversationId);
        PubnubBroadcast.unsubscribe(channel);
        PubnubBroadcast.subscribe(channel, new CallbackResponse() {

            @Override
            public void onSuccess(JSONObject json) {
                chatMessage2 = Parser.parseChatMessage(json);
                if( chatMessage2 == null ) return;
                runThread(new Action() {
                    @Override
                    void action() {
                        receiveMessageOrNotify(start);
                    }
                });
            }
        });
        notif = true;
    }

    private void getMessages(String conversationId) {
        retreiveConversation(conversationId);
    }

    private Comparator<ChatMessage> getComparator() {
        return new Comparator<ChatMessage>() {
            @Override
            public int compare(ChatMessage lhs, ChatMessage rhs) {
                return lhs.dateLong > rhs.dateLong ? 1 :
                        lhs.dateLong < rhs.dateLong ? -1 : 0 ;
            }
        };
    }

    private void receiveMessageOrNotify(boolean start) {
        if( !start ) {
            Intent intent = ChatView.this.getIntent();
            intent.putExtra(convId, conversationId);
            PubnubBroadcast.notify(intent, "nouveau message", chatMessage2.body);
        } else {
            chatAdapter.add(chatMessage2);
            chatAdapter.notifyDataSetChanged();
        }
    }

    private void runThread(final Action a) {
        new Thread() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            a.action();
                        }
                    });
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        start = true;
    }

    @Override
    protected void onStop() {
        start = false;
        super.onStop();

    }

    @Override
    protected void onPause() {
        start = false;
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        start = true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        start = true;
    }

    @Override
    public void onBackPressed() {
        start = false;
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendMessageButton:
                sendTextMessage(v);
        }
    }

    private abstract class Action {
        void action() {
        }
    }

    private void triListe(){
        ChatMessage[] liste =  new ChatMessage[chatlist.size()];
    }
}
