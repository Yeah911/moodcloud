package com.moodcloud.moodcloud.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class FriendRequest extends AppCompatActivity {

    private List<String> elements = new ArrayList<>();
    private List<User> users;
    private List<String> requestsId;
    private ListView list;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);

        list = (ListView) findViewById(R.id.listViewFriendRequest);
        adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, elements);
        list.setAdapter(adapter);

        getRequests();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final int pos = position;
                new AlertDialog.Builder(FriendRequest.this)
                        .setMessage("add friend ?")
                        .setCancelable(false)
                        .setPositiveButton("Accepter", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                acceptFriend(pos);
                            }
                        })
                        .setNegativeButton("Bloquer", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                blockFriend(pos);
                            }
                        })
                        .setNegativeButton("Refuser", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                refuseFriend(pos);
                            }
                        })
                        .show();
            }
        });
    }

    private void getRequests() {


        AsyncResponse.sendRequestGet(Connection.URLRedirection.friendRequester, new CallbackResponse(this) {
            @Override
            public void onSuccess(JSONArray json) {
                requestsId = new ArrayList<>();
                users = Parser.parseAskUsers(json, requestsId);
                for( User u : users ) {
                    elements.add(u.getLogin());
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onSuccess() {
                Utils.toast(context, "aucune nouvelle demande");
            }
        });
    }

    private void acceptFriend(final int position){
        RequestParams rp = new RequestParams();
        rp.put("requestID", requestsId.get(position));
        AsyncResponse.sendRequestGet(Connection.URLRedirection.acceptFriend, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                elements.remove(position);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void refuseFriend(final int position){
        RequestParams rp = new RequestParams();
        rp.put("requestID", requestsId.get(position));
        AsyncResponse.sendRequestGet(Connection.URLRedirection.refuseFriend, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                elements.remove(position);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void blockFriend(final int position){
        RequestParams rp = new RequestParams();
        rp.put("requestID", requestsId.get(position));
        AsyncResponse.sendRequestGet(Connection.URLRedirection.blockFriend, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                elements.remove(position);
                adapter.notifyDataSetChanged();
            }
        });
    }
}
