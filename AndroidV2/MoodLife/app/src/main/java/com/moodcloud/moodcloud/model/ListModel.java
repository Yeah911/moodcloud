package com.moodcloud.moodcloud.model;

import java.util.List;

/**
 * Created by Yehouda on 26/06/2016.
 */
public class ListModel {

    public ListModel(String id, String name, List<User> users) {
        this.id = id;
        this.name = name;
        this.users = users;
    }

    @Override
    public String toString() {
        return name;
    }

    public String id;
    public String name;
    public List<User> users;
}
