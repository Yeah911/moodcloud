package com.moodcloud.moodcloud.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;

import com.moodcloud.moodcloud.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yehouda on 26/05/2016.
 */
public class NotificationCreator {

    public NotificationCreator(Context context, Intent intent, AppCompatActivity activity) {
        this.builder = new NotificationCompat.Builder(context);
        this.context = context;
        this.intent = intent;
        this.activity = activity;
        pendingIntent = PendingIntent.getActivity( context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    public NotificationCreator(AppCompatActivity activity, Intent intent) {
        this(activity.getApplicationContext(), intent, activity);
    }

    public void addAction(int icon, String title) {
        addAction(icon, title, pendingIntent);
    }

    public void addAction(int icon, String title, PendingIntent intent) {
        if( actions == null ) {
            actions = new ArrayList<>();
        }
        actions.add( new Action(icon, title, intent) );
    }

    public void addAction(int icon, String title, Intent intent) {
        addAction(icon, title, PendingIntent.getActivity( context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT )) ;
    }

    public void create(int icon, String title, String text) {
        builder.setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true);
        if( actions != null && ! actions.isEmpty() ) {
            for( Action a : actions ) {
                builder.addAction(a.icon, a.title, a.pendingIntent);
            }
        }
        builder.setContentIntent(pendingIntent);
        NotificationManager mNotifyMgr =
                (NotificationManager) activity.getSystemService(activity.NOTIFICATION_SERVICE);
        mNotifyMgr.notify((int) System.currentTimeMillis(), builder.build());
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public PendingIntent getPendingIntent() {
        return pendingIntent;
    }

    public void setPendingIntent(PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
    }

    private NotificationCompat.Builder builder;
    private List<Action> actions;
    private Intent intent;
    private AppCompatActivity activity;
    private PendingIntent pendingIntent;
    private Context context;

    class Action {
        public Action(int icon, String title, PendingIntent intent) {
            this.icon = icon;
            this.title = title;
            this.pendingIntent = intent;
        }

        private int icon;
        private String title;
        private PendingIntent pendingIntent;
    }

}
