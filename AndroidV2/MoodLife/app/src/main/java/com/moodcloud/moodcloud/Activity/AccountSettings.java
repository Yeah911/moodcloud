package com.moodcloud.moodcloud.Activity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.moodcloud.moodcloud.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AccountSettings extends AppCompatActivity   implements View.OnClickListener{

    private EditText toDateEtxt;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        Button up = (Button) findViewById(R.id.update);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy",Locale.FRENCH);

        findViewsById();

        setDateTimeField();
    }


    private void findViewsById() {
        toDateEtxt = (EditText) findViewById(R.id.bdaydate);
        toDateEtxt.setInputType(InputType.TYPE_NULL);
    }

    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();
        toDateEtxt.setOnClickListener(this);
        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    @Override
    public void onClick(View view) {
       if(view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }

    private void updateData()
    {
        finish();
    }
}
