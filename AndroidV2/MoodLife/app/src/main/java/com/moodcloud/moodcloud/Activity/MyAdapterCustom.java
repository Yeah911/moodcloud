package com.moodcloud.moodcloud.Activity;

/**
 * Created by 626 on 22/06/2016.
 */

    import android.content.Context;
    import android.support.v7.widget.CardView;
    import android.support.v7.widget.RecyclerView;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.TextView;

    import com.loopj.android.http.RequestParams;
    import com.moodcloud.moodcloud.R;
    import com.moodcloud.moodcloud.model.Conversation;
    import com.moodcloud.moodcloud.model.Message;
    import com.moodcloud.moodcloud.model.MoodMessage;
    import com.moodcloud.moodcloud.remote.AsyncResponse;
    import com.moodcloud.moodcloud.remote.CallbackResponse;
    import com.moodcloud.moodcloud.remote.Connection;
    import com.moodcloud.moodcloud.remote.Parser;

    import org.json.JSONArray;
    import org.json.JSONObject;

    import java.util.ArrayList;
    import java.util.List;

public class MyAdapterCustom extends RecyclerView.Adapter<MyAdapterCustom.MyViewHolder> {
        private List<Message> mDataset;
        private List<String> mSentence;
        private Context context;
        private String moodId;
        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder {
            public CardView mCardView;
            public TextView mTextView;
            public TextView mTextView2;
            public MyViewHolder(View v) {
                super(v);

                mCardView  = (CardView) v.findViewById(R.id.card_view);
                mTextView  = (TextView) v.findViewById(R.id.tv_text);
                mTextView2 = (TextView) v.findViewById(R.id.firstSentence);
            }
        }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapterCustom(Context ctx, String moodId) {
        mDataset = new ArrayList<>();
        mSentence = new ArrayList<>();
        context = ctx;
        this.moodId = moodId;
        retreiveMoodConversation();
    }

    private void retreiveMoodConversation() {
        RequestParams rp = new RequestParams();
        rp.put("moodId", moodId);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.retrieveMoodConversations, rp, new CallbackResponse() {
            @Override
            public void onSuccess(JSONObject json) {
                List<Conversation> conversations = Parser.parseConversation( json );
                for(Conversation conv : conversations) {
                    retreiveConversation(conv.getId());
                }
            }
        });
    }

    private void retreiveConversation(String convId) {
        RequestParams rp = new RequestParams();
        rp.put("conversationId", convId);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.retreiveConversation, rp, new CallbackResponse() {
            @Override
            public void onSuccess(JSONObject json) {
                Message message = Parser.parseMessagesConversation(json);
                mDataset.add(message);
                List<MoodMessage> messages = message.getMessages();
                if( messages.size() >= 1 ) {
                    String content = messages.get(messages.size() - 1).getContent();
                    mSentence.add(content);
                } else {
                    mSentence.add("...");
                }
                notifyDataSetChanged();
            }
        });
    }



        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapterCustom.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_item, parent, false);
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final Message message = mDataset.get(position);
            holder.mTextView.setText(message.getFrom().getLogin());
            holder.mTextView2.setText(mSentence.get(position));

            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof Chat){
                        ((Chat)context).startConversation(message);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }

    }

