package com.moodcloud.moodcloud.model;

public class FriendRelation {

    public FriendRelation() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdAsker() {
        return idAsker;
    }

    public void setIdAsker(String idAsker) {
        this.idAsker = idAsker;
    }

    public String getIdAsked() {
        return idAsked;
    }

    public void setIdAsked(String idAsked) {
        this.idAsked = idAsked;
    }

    public boolean isFriend() {
        return friend;
    }

    public void setFriend(boolean friend) {
        this.friend = friend;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    private String id;
    private String idAsker;
    private String idAsked;
    private boolean friend;
    private boolean blocked;
}
