package com.moodcloud.moodcloud.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.model.ListModel;
import com.moodcloud.moodcloud.model.Mood;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.Request.MoodRequests;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class Mooder extends AppCompatActivity implements View.OnClickListener, LocationListener {
    private RatingBar ratingBar;
    private TextView tvRating;
    private EditText comment;
    private LayerDrawable stars;
    private Button btn;
    private AlertDialog dialog;
    RadioButton customRB;
    private List<String> elementsListContact = new ArrayList<>();
    private List<ListModel> listFriends = new ArrayList<>();
    private ArrayAdapter<String> adapterListContact;
    private ListModel listShare = null;
    private double  latitude, longitude;
    private float rate;
    private LocationManager locationManager;
    private View loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mooder);
        loading = findViewById(R.id.loadingPanel);
        loading.setVisibility(View.GONE);
        setListFriends();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean accessLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean coarseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        comment = (EditText) findViewById(R.id.moodComment);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        tvRating = (TextView) findViewById(R.id.value);
        stars = (LayerDrawable) ratingBar.getProgressDrawable();
        ratingBar.setRating(0.0f);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float ratingValue,
                                        boolean fromUser) {

                int value = (int) (ratingValue) - 9;
                if (value == 11)
                    value--;
                tvRating.setText(String.valueOf(value));

                int color = Color.BLUE;

                if (value > 0)
                    color = Color.GREEN;
                else if (value < 0)
                    color = Color.RED;

                stars.getDrawable(2).setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
        });
        btn = (Button) findViewById(R.id.mooder);
        btn.setOnClickListener(this);
        customRB = (RadioButton) findViewById(R.id.customMood);
        customRB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //TODO traitement bdd
        switch (v.getId()) {
            case R.id.mooder:
                sendRequestMooder();
                break;
            case R.id.customMood:
                RadioButton prive = (RadioButton) findViewById(R.id.privatemood);
                prive.setChecked(false);
                if (customRB.isChecked()) {
                    if (listFriends.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "vous n'avez pas créer de list d'amis", Toast.LENGTH_SHORT).show();
                        RadioButton pr = (RadioButton) findViewById(R.id.privatemood);
                        pr.setChecked(true);
                    } else {
                        getListFriends();
                    }
                }
                break;
        }
    }

    private void getListFriends() {
        final LayoutInflater li = LayoutInflater.from(getBaseContext());
        final View customView = li.inflate(R.layout.dialog_add_list_or_delete, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(Mooder.this);
        this.dialog = builder.create();

        customView.findViewById(R.id.textView7).setVisibility(View.GONE);
        customView.findViewById(R.id.remove).setVisibility(View.GONE);

        final TextView textView = (TextView) customView.findViewById(R.id.textView8);
        textView.setText("Partagez avec vos amis");
        final ListView list = (ListView) customView.findViewById(R.id.listFriendsToAdd);
        elementsListContact.clear();
        for (ListModel ml : listFriends) {
            elementsListContact.add(ml.name);
        }
        adapterListContact = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, elementsListContact);
        list.setAdapter(adapterListContact);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (ListModel lm : listFriends) {
                    if (lm.name.equals(elementsListContact.get(position))) {
                        listShare = lm;
                        dialog.dismiss();
                        break;
                    }
                }
            }
        });

        dialog.setView(customView);
        dialog.setTitle("gestion liste");
        dialog.setMessage("nom de list");
        dialog.setCancelable(true);
        dialog.show();
    }

    private void setListFriends() {

        if (!listFriends.isEmpty()) {
            return;
        }

        AsyncResponse.sendRequestGet(Connection.URLRedirection.broadcastLists, new CallbackResponse(this) {
            @Override
            public void onSuccess(JSONArray json) {
                listFriends.clear();
                listFriends.addAll(Parser.parseListFriends(json));
            }
        });
    }

    private void sendRequestMooder() {
        int values = (int) ratingBar.getRating() - 9;
        if (values == 11)
            rate = ratingBar.getRating() - 10;
        else
            rate = ratingBar.getRating() - 9;

        SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
        RequestParams rp = new RequestParams();
        rp.put("content", comment.getText().toString() + ".");
        rp.put("rate", (int) rate);
        RadioButton publicbutton = (RadioButton) findViewById(R.id.publicmood);
        RadioButton custombutton = (RadioButton) findViewById(R.id.customMood);
        int type = 0;
        if (publicbutton.isChecked()) {
            type = 2;
        } else if (custombutton.isChecked()) {
            type = 1;
        }
        rp.put("type", type);
        Calendar cal = Calendar.getInstance();
        rp.put("creationDate", dateFormatter.format(cal.getTime()));
        Connection.URLRedirection redirection;
        boolean loc = false;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

         return;
        }
        else
            onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

        if( longitude != 0.0 && latitude != 0.0 ) {
            String convers = String.valueOf(longitude);
            convers = convers.replace(".",",");
            rp.put("longitude", convers);
            String convers2 = String.valueOf(latitude);
            convers2 = convers2.replace(".",",");
            rp.put("latitude", convers2);
            loc = true;
        }
        if( listShare == null ) {
            redirection = Connection.URLRedirection.mooder;
            if( loc )
                redirection = Connection.URLRedirection.createMoodWithLocalization;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("([");
            int i = 0;
            for( User usr : listShare.users ) {
                if( i++ > 0 ) {
                    sb.append(", ");
                }
                sb.append("'").append(usr.getId()).append("'");
            }
            sb.append("])");
            rp.put("destinators", sb.toString());
            redirection = Connection.URLRedirection.shareMooder;
            if( loc )
                redirection = Connection.URLRedirection.shareMoodWithLocalization;
        }
        AsyncResponse.sendRequestPost(redirection, rp, moodCallback());
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    private CallbackResponse moodCallback() {
        return new CallbackResponse() {

            @Override
            public void onSuccess(JSONObject json) {
                int a  = 0 ;
                String id = null;
                try {
                    id = json.getString("Id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Mood mood = new Mood((int) rate, id, comment.getText().toString());
                MoodRequests moodRequests = new MoodRequests(getApplicationContext(), Informations.user);
                moodRequests.add(mood);
                Toast.makeText(getApplicationContext(), "Mood ajouté", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Connection.URLRedirection redirect, Throwable error) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                try {
                    String value = new String(responseBody, "UTF-8");
                    System.out.println(value);
                    Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                    finish();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

            public void onFailure(Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                finish();
            }
        };
    }

        @Override
        public void onLocationChanged(Location loc) {
            if(loc == null)
                return;
            latitude = (float) loc.getLatitude();
            longitude = (float) loc.getLongitude();
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
}
