package com.moodcloud.moodcloud.model;

import com.moodcloud.moodcloud.remote.Parser;

import java.io.Serializable;
import java.util.Date;

public class Mood implements Serializable{

    public Mood() {}

    public Mood(float mark) {
        this.mark = mark;
    }

    public Mood(float mark, String comment) {
        this.mark = mark;
        this.comment = comment;
    }

    public Mood(float mark, String guid, String comment) {
        this.mark = mark;
        this.guid = guid;
        this.comment = comment;
    }

    public Mood(String guid, float mark, String comment, MoodType type, String state, Date date) {
        this.mark = mark;
        this.guid = guid;
        this.comment = comment;
        this.type = type;
        this.state = state;
        this.date = date;
    }

    public Mood(String guid, float mark, String comment, MoodType type, String state, Date date, long dateLong) {
        this.mark = mark;
        this.guid = guid;
        this.comment = comment;
        this.type = type;
        this.state = state;
        this.date = date;
        this.dateLong = dateLong;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public MoodType getType() {
        return type;
    }

    public void setType(MoodType type) {
        this.type = type;
    }

    public void setType(String typeMood) {
        typeMood = typeMood.toLowerCase();
        if( typeMood.equals("privatemood") ) {
            type = Mood.MoodType.privateMood;
        } else if( typeMood.equals("publicmood") ) {
            type = Mood.MoodType.publicMood;
        } else {
            type = Mood.MoodType.protectedMood;
        }
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getDateLong() {
        return dateLong;
    }

    public void setDateLong(long dateLong) {
        this.dateLong = dateLong;
    }

    private float mark;
    private String guid;
    private String comment;
    private MoodType type;
    private String state;
    private long dateLong;
    private Date date;


    public enum MoodType{
        privateMood,
        protectedMood,
        publicMood
    };

}
