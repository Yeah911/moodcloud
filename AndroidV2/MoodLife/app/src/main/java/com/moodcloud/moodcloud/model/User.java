package com.moodcloud.moodcloud.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.persistance.Request.SqlRequest;

import java.io.Serializable;

public class User implements Serializable{

    public User() {
    }

    public User(String id) {
        this.id = id;
    }

    public User(String id, String login, String email, String phone) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.phone = phone;
    }

    public User(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String id;
    private String login;
    private String phone;
    private String password;
    private String email;
}
