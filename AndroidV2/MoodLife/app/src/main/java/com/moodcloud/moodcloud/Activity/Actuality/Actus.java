package com.moodcloud.moodcloud.Activity.Actuality;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.moodcloud.moodcloud.Activity.ChatView;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.Message;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Actus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actus);
        RecyclerView recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        ContactAdapter ca = new ContactAdapter(Actus.this);
        recList.setAdapter(ca);
    }
    public void startConversation(Message message,boolean from){
        Intent intent = new Intent(this,ChatView.class);
        intent.putExtra("message", message);
        intent.putExtra("origin",from);
        startActivity(intent);
    }
}
