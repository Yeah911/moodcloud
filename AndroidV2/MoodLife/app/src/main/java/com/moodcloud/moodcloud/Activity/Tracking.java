package com.moodcloud.moodcloud.Activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.db.chart.model.LineSet;
import com.db.chart.view.AxisController;
import com.db.chart.view.LineChartView;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.persistance.Request.SqlRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Tracking extends AppCompatActivity implements View.OnClickListener {

    //UI References
    private EditText fromDateEtxt;
    private EditText toDateEtxt;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    LineSet dataset;
    private DAOAccess access;
    private final String name_table = "mood";
    private LineChartView lc;
    private int compteur,total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lc = (LineChartView) findViewById(R.id.linechart);
        access = new DAOAccess(getApplicationContext(), name_table, SqlRequest.create_mood_tables);

        access.open();

        ArrayList<String> array = new ArrayList<String>();
        ArrayList<Float> value = new ArrayList<Float>();
        Cursor cursor = access.get(SqlRequest.select_mood_rate, new String[] {Informations.user.getId()});

        try
        {
            compteur = total = 0;
            while (cursor.moveToNext())
            {
                array.add("label");
                int res = (cursor.getInt(0));
                total += res;
                value.add(new Float ((float) res));
                compteur++;
            }
        }
        finally
        {
            cursor.close();
            access.close();
        }

        String[] myStrings = new  String[array.size()];
                myStrings = array.toArray(myStrings);
        float[] myIntArray = new float[value.size()];
        int i = 0;

        for (Float f : value) {
            myIntArray[i++] = (f != null ? f : Float.NaN); // Or whatever default you want.
        }

        String[] myStrings2 = new  String[array.size()];

        for (int j = array.size()-1,l=0; j >=0; j--,l++) {
            myStrings2[l] = myStrings[j]; // Or whatever default you want.
        }
        float[] myIntArray2 = new float[value.size()];
        for (int j = value.size()-1,l=0; j >=0; j--,l++) {
            myIntArray2[l] = myIntArray[j]; // Or whatever default you want.
        }

        dataset = new LineSet(myStrings2, myIntArray2);
        dataset.setColor(Color.parseColor("#d58802"));
        lc.addData(dataset);
        lc.setYAxis(false);
        lc.setXAxis(false);
        lc.setYLabels(AxisController.LabelPosition.NONE);
        lc.setXLabels(AxisController.LabelPosition.NONE);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
        findViewsById();
        setDateTimeField();

        setMoyenne();
        lc.show();
    }

    private void findViewsById() {
        fromDateEtxt = (EditText) findViewById(R.id.etxt_fromdate);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

        toDateEtxt = (EditText) findViewById(R.id.bdaydate);
        toDateEtxt.setInputType(InputType.TYPE_NULL);
    }

    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);
        toDateEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                reload();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    @Override
    public void onClick(View view) {
        if(view == fromDateEtxt) {
            fromDatePickerDialog.show();
        } else if(view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }
    public void reload()
    {
        access = new DAOAccess(getApplicationContext(), name_table, SqlRequest.create_mood_tables);
        access.open();

        ArrayList<String> array = new ArrayList<String>();
        ArrayList<Float> value = new ArrayList<Float>();

        String dateFrom = fromDateEtxt.getText().toString() + " 00:00:00";
        String dateTo = toDateEtxt.getText().toString() + " 23:59:59";

        System.out.println(dateFrom);
        System.out.println(dateTo);
        Cursor cursor = access.get(SqlRequest.select_mood_rate_range, new String[] {dateFrom,dateTo, Informations.user.getId()});
        String[] myStrings;
        float[] myIntArray;
        try
        {
            while (cursor.moveToNext())
            {
                array.add("label");
                int res = (cursor.getInt(0));
                value.add(new Float ((float) res));

                total += res;
                compteur++;
            }

        }
        finally
        {
            cursor.close();
        }
        myStrings = new  String[array.size()];
        myStrings = array.toArray(myStrings);
        myIntArray = new float[value.size()];
        int i = 0;

        for (Float f : value) {
            myIntArray[i++] = (f != null ? f : Float.NaN); // Or whatever default you want.
        }
        String[] myStrings2 = new  String[array.size()];

        for (int j = array.size()-1 , l=0; j >=0; j--, l++) {
            myStrings2[l] = myStrings[j]; // Or whatever default you want.
        }
        float[] myIntArray2 = new float[value.size()];
        for (int j = value.size()-1,l=0; j >=0; j--,l++) {
            myIntArray2[l] = myIntArray[j]; // Or whatever default you want.
        }

        //todo requete
        lc.dismiss();
        dataset = new LineSet(myStrings2, myIntArray2);
        dataset.setColor(Color.parseColor("#d58802"));
        lc.addData(dataset);

        setMoyenne();
        lc.show();

    }

    private void setMoyenne(){
        TextView moyenne = (TextView) findViewById(R.id.average);

        float moy = ( compteur != 0 ) ? (float) total/compteur : 0 ;
        String mytext= "Moyenne de votre humeur : " + (Float.toString(moy));
        moyenne.setText(mytext);
    }
}
