package com.moodcloud.moodcloud.persistance.Request;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.util.Utils;

public class MoodUnsendRequests {

    public MoodUnsendRequests(Context context) {
        access = new DAOAccess(context, name_table, SqlRequest.create_mood_unsend);
    }

    public void add(int idMood) {
        access.open();
        ContentValues cv = new ContentValues();
        cv.put("id_mood", idMood);
        access.add(name_table, cv);
        access.close();
    }

    public String get(int idMood) {
        access.open();
        Cursor cursor = access.get(SqlRequest.select_mood_unsend, new String[] {idMood +""});
        if ( !cursor.moveToNext() ) {
            return null;
        }
        String id = cursor.getString(0);
        access.close();
        return id;
    }

    public void remove(int idMood) {
        remove( get(idMood) );

    }

    public void remove(String id) {
        access.open();
        access.delete(name_table, id);
        access.close();
    }

    private DAOAccess access;
    private final String name_table = "mood";
}
