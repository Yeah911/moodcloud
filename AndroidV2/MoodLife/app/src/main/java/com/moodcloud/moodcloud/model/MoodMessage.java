package com.moodcloud.moodcloud.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Yehouda on 28/06/2016.
 */
public class MoodMessage implements Serializable {

    public MoodMessage() {
    }

    public MoodMessage(String id, String content, Date dateCreate, User author) {
        this.id = id;
        this.content = content;
        this.dateCreate = dateCreate;
        this.author = author;
    }

    public MoodMessage(String id, String content, Date dateCreate, long dateCreateLong, User author) {
        this.id = id;
        this.content = content;
        this.dateCreate = dateCreate;
        this.dateCreateLong = dateCreateLong;
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public long getDateCreateLong() {
        return dateCreateLong;
    }

    public void setDateCreateLong(long dateCreateLong) {
        this.dateCreateLong = dateCreateLong;
    }

    private String id;
    private String content;
    private Date dateCreate;
    private long dateCreateLong;
    private User author;
}
