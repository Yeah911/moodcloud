package com.moodcloud.moodcloud.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.moodcloud.moodcloud.Activity.Actuality.Actus;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.applications.PubnubBroadcast;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.persistance.Request.HistoryRequests;
import com.moodcloud.moodcloud.persistance.Request.SqlRequest;
import com.moodcloud.moodcloud.persistance.Request.UserRequests;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private UserRequests usrRequest;
    private User usr;
    HistoryRequests hr;
    private DAOAccess access;
    private final String name_table = "mood";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i ;

        hr = new HistoryRequests(getApplicationContext());
        if(!hr.getLastIsAnAutoLog())
        {
            i = new Intent(this, Login.class);
            startActivity(i);
        }
        else
        {
            sendReconnect();
        }

        Button mooder = (Button) findViewById(R.id.mooder);
        Button trackLog = (Button) findViewById(R.id.tracking);

        mooder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Mooder.class);
                startActivity(i);
            }
        });

        trackLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Tracking.class);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(hr.getLastIsAnAutoLog())
            initComment();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //initComment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_actualites) {
            Intent i ;
            i = new Intent(this, Actus.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.action_settings) {
            Intent i ;
            i = new Intent(this, AccountSettings.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.logout) {
            hr.updateLastUser();
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            return true;
        }
        if (id == R.id.conversation) {
            Intent i = new Intent(this, Chat.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.action_friend) {
            Intent i = new Intent(this, FriendRequest.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.contact_settings) {
            Intent i = new Intent(this, Contacts.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initComment(){
        access = new DAOAccess(getApplicationContext(), name_table, SqlRequest.create_mood_tables);
        access.open();

        ArrayList<String> array = new ArrayList<String>();
        Cursor cursor = access.get(SqlRequest.select_mood_rate, new String[] {Informations.user.getId()});

        try
        {
            String commentaires = "";

            while (cursor.moveToNext())
            {
                commentaires = cursor.getString(1);
                if(!commentaires.isEmpty() )
                    array.add(commentaires);
            }
        }
        finally
        {
            cursor.close();
            access.close();
        }
        String myStrings2 = " ";

        for (int j = array.size()-1,l=0; j >=0; j--,l++) {
            myStrings2 += "#"+array.get(j) +" / "; // Or whatever default you want.
        }
        TextView moodcomment = (TextView) findViewById(R.id.moodrecap);
        moodcomment.setText(myStrings2);
    }

    private void sendReconnect(){
        AsyncResponse.sendRequestGet(Connection.URLRedirection.challenge, null, new CallbackResponse() {
            @Override
            public void onSuccess(String challenge) {
                Map<String, String> rp = new HashMap<>();

                String hash = Utils.encryptMD5( challenge + Informations.user.getPassword());
                rp.put("userId", Informations.user.getEmail());
                rp.put("secret", hash);
                AsyncResponse.sendRequestGet(Connection.URLRedirection.authenticate, rp, null, new CallbackResponse() {

                    @Override
                    public void onSuccess(boolean connected) {
                    findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    if(connected) {
                        PubnubBroadcast.subscribePrivateChannel(MainActivity.this, new Intent(MainActivity.this, Chat.class));
//                        PubnubBroadcast.getChannelToSubscribe();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Connect error", Toast.LENGTH_LONG).show();
                    }
                    }
                });

            }
        });
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }
}
