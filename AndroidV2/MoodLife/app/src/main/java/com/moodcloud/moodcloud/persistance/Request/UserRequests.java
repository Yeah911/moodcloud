package com.moodcloud.moodcloud.persistance.Request;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class UserRequests {

    public UserRequests(Context context) {
        this.context = context;
        access = new DAOAccess(context, name_table, SqlRequest.create_user_tables);
    }

    public void register(User usr) {
        access.open();
        ContentValues cv = new ContentValues();
        String password = Utils.encryptMD5(usr.getPassword(),false);
        cv.put("login", usr.getLogin());
        cv.put("password", password);
        cv.put("email", usr.getEmail());
        access.add(name_table, cv);
        access.close();
    }

    public User getUser(String id) {
        User usr = new User(id);
        access.open();
        Cursor cursor = access.get(SqlRequest.select_user_by_id, new String[] {id});
        cursor.moveToNext();
        usr.setLogin(cursor.getString(1));
        usr.setPassword(cursor.getString(2));
        usr.setEmail(cursor.getString(3));
        return usr;
    }

    public User getUser(String email, String password) {
        User usr = new User();
        access.open();
        password = Utils.encryptMD5(password,false);
        Cursor cursor = access.get(SqlRequest.select_existing_user, new String[] {email, password});
        if (! cursor.moveToNext() ) {
            return null;
        }
        usr.setId(cursor.getString(0));
        usr.setLogin(cursor.getString(1));
        usr.setPassword(cursor.getString(2));
        usr.setEmail(cursor.getString(3));
        return usr;
    }

    public User getUserByEmail(String email) {
        User usr = new User();
        access.open();
        Cursor cursor = access.get(SqlRequest.select_user_by_email, new String[] {email});
        if (! cursor.moveToNext() ) {
            return null;
        }
        usr.setId(cursor.getString(0));
        usr.setLogin(cursor.getString(1));
        usr.setPassword(cursor.getString(2));
        usr.setEmail(cursor.getString(3));
        return usr;
    }

    public User getUserByLogin(String login) {
        User usr = new User();
        access.open();
        Cursor cursor = access.get(SqlRequest.select_user_by_login, new String[] {login});
        if (! cursor.moveToNext() ) {
            return null;
        }
        usr.setId(cursor.getString(0));
        usr.setLogin(cursor.getString(1));
        usr.setPassword(cursor.getString(2));
        usr.setEmail(cursor.getString(3));
        return usr;
    }

    public List<User> getFriends(User usr) {
        FriendRequests friends = new FriendRequests(context);
        return friends.getFriends(usr);
    }

    public List<User> getUser() {
        List<User> users = new ArrayList<>();
        access.open();
        Cursor cursor = access.get(SqlRequest.select_user, new String[] {"1"});
        while ( cursor.moveToNext() ) {
            User usr = new User();
            usr.setId(cursor.getString(0));
            usr.setLogin(cursor.getString(1));
            usr.setPassword(cursor.getString(2));
            usr.setEmail(cursor.getString(3));
            users.add(usr);
        }
        return users;
    }

    private DAOAccess access;
    private final String name_table = "user";
    private Context context;

}
