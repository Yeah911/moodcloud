package com.moodcloud.moodcloud.model;

import com.moodcloud.moodcloud.util.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yehouda on 28/06/2016.
 */
public class Conversation implements Serializable {

    public Conversation(String id, int messageCount, Date expire, Date lastUpdate) {
        this.id = id;
        this.messageCount = messageCount;
        this.expire = expire;
        this.lastUpdate = lastUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    private String id;
    private int messageCount;
    private Date expire;
    private Date lastUpdate;
}
