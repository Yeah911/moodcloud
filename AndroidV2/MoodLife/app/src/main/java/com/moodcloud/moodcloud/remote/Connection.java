package com.moodcloud.moodcloud.remote;

import android.content.Context;
import android.os.Looper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.moodcloud.moodcloud.applications.Informations;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;

public class Connection {

    public static final String DOMAIN = "devmoodcloudwebapp.azurewebsites.net";
    //public static final String DOMAIN = "10.0.2.2";
    private static final String BASE_URL = "http://" + DOMAIN + "/MoodCloud/";
    //private static final String BASE_URL = "http://" + DOMAIN + "/WebHost/MoodCloud/";
    private static AsyncHttpClient client = new AsyncHttpClient();
    private static SyncHttpClient syncClient = new SyncHttpClient();
    public static final String cookieName = "MoodCloud-AspectizeDC3" ;

    public static enum URLRedirection {
        // SecurityServices
        challenge   ( "SecurityServices.GetAuthenticationChallenge" ),
        authenticate( "SecurityServices.Authenticate" ),
        // AuthenticationService
        associateProfile ("AuthenticationService.GetUserProfile"),
        getPrivateChannel("AuthenticationService.GetPrivateChannel"),
        // InscriptionService
        register( "InscriptionService.SignUp" ),
        // ConversationService
        sendMessage              ("ConversationService.SendMessage"),
        retreiveConversation     ("ConversationService.RetrieveConversationJS"),
        retrieveMoodConversations("ConversationService.RetrieveMoodConversationsJS"),
        createMoodConversations  ("ConversationService.CreateConversationJS"),
        // MoodService
        mooder                    ("MoodService.CreateMoodJS"),
        shareMooder               ("MoodService.CreateMoodAndPublishJS"),
        retreiveUserMoods         ("MoodService.RetrieveAllUserMoodsJS"),
        retreiveNewsFeed          ("MoodService.RetrieveNewsFeedJS"),
        shareMoodWithLocalization ("MoodService.CreateMoodWithLocalizationAndPublishJS"), //string content, short rate, EnumMoodType type, DateTime creationDate, double longitude, double latitude, params Guid[] destinators
        createMoodWithLocalization("MoodService.CreateMoodWithLocalizationJS"),//string content, short rate, EnumMoodType type, DateTime creationDate, double longitude, double latitude
        // BroadcastListService
        broadcastLists            ("BroadcastListService.GetBroadcastListsOwnedJS"),
        createBroadcastLists      ("BroadcastListService.CreateBroadcastListJS"),
        addFriendBroadcastLists   ("BroadcastListService.AddIntoBroadcastList"),
        removeFriendBroadcastLists("BroadcastListService.RemoveFromBroadcastList"),
        deleteBroadCastList       ("BroadcastListService.DeleteBroadCastList"),
        subscribeChannel          ("BroadcastListService.GetBroadcastListsToSuscribeJS" ),
        // FriendshipService
        friendRequester("FriendshipService.RetrieveFriendshipRequestJS"),
        acceptFriend   ("FriendshipService.AcceptFriendRequest"),
        refuseFriend   ("FriendshipService.RejectFriendRequest"),
        removeFriend   ("FriendshipService.BreakFriendship"),
        blockFriend    ("FriendshipService.BlockUserRequests"),
        searchUser     ("FriendshipService.SearchUser"),
        requestFriend  ("FriendshipService.SendFriendRequest"),
        getFriends     ("FriendshipService.RetrieveFriendsJS")
        ;

        URLRedirection(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        private String url;
    };


    private static PersistentCookieStore myCookieStore = null;

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        getClient().get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void getSync(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        syncClient.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void exchangeAspectizeCookie(Header[] headers) {
        for (Header head : headers) {
            String value = head.getValue();
            if ( head.getName().equals( "Set-Cookie" )  && value.startsWith( Connection.cookieName ) ) {
                Informations.cookieAspectize = value;
                break;
            }
        }
        client.addHeader("Cookie", Informations.cookieAspectize);
        syncClient.addHeader("Cookie", Informations.cookieAspectize);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        getClient().post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public static void persistCookie(Context context, String value) {
        persistCookie(context, cookieName, value, DOMAIN, "/", 1);
    }

    public static void persistCookie(Context context, String name, String value) {
        persistCookie(context, name, value, DOMAIN, "/", 1);
    }

    public static void persistCookie(Context context, String name, String value, String domain) {
        persistCookie(context, name, value, domain, "/", 1);
    }

    public static void persistCookie(Context context, String name, String value, String domain, String path) {
        persistCookie(context, name, value, domain, path, 1);
    }

    public static void persistCookie(Context context, String name, String value, String domain, String path, int version) {
        if( myCookieStore == null ) {
            myCookieStore = new PersistentCookieStore(context);
            client.setCookieStore(myCookieStore);
        }

        BasicClientCookie newCookie = new BasicClientCookie(name, value);
        newCookie.setVersion(1);
        newCookie.setDomain(domain);
        newCookie.setPath(path);
        myCookieStore.addCookie(newCookie);
    }

    public static List<Cookie> getCookies() {
        return myCookieStore.getCookies();
    }

    public static Cookie getCookie(String name) {
        List<Cookie> cookies =  myCookieStore.getCookies();
        for( Cookie c : cookies ) {
            if( c.getName().equals(name) )
                return c;
        }
        return null;
    }

    public static String getCookieValue(String name) {
        List<Cookie> s =  myCookieStore.getCookies();
        for( Cookie c : s ) {
            if( c.getName().equals(name) )
                return c.getValue();
        }
        return null;
    }


    private static AsyncHttpClient getClient() {
        // Return the synchronous HTTP client when the thread is not prepared
        if (Looper.myLooper() == null)
            return syncClient;
        return client;
    }
    //http://localhost/WebHost/MoodCloud/SecurityServices.GetAuthenticationChallenge.json.cmd.ashx
    //http://localhost/WebHost/MoodCloud/SecurityServices.Authenticate.json.cmd.ashx?userName=&secret=
}
