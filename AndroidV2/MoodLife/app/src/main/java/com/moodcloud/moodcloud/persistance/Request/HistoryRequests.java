package com.moodcloud.moodcloud.persistance.Request;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.util.Utils;

import java.sql.Date;


public class HistoryRequests {

    public HistoryRequests(Context context) {
        access = new DAOAccess(context, name_table, SqlRequest.create_history_tables);
        this.context = context;
    }

    public void add(User usr, String action) {
        add(usr, action, false);
    }

    public void add(User usr, String action, boolean autoLog) {
        access.open();
        ContentValues cv = new ContentValues();
        cv.put("action", action);
        cv.put("id_user", usr.getId());
        cv.put("date", Utils.getCurrentDate());
        cv.put("auto_login", autoLog);
        access.add(name_table, cv);
        access.close();
    }

    public void updateLastUser()
    {
        this.add(Informations.user, "login", false);
    }

    public boolean getLastIsAnAutoLog() {
        access.open();
        boolean autoLog = false;
        Cursor cursor = access.get(SqlRequest.select_last_histoy, null);
        if( !cursor.moveToNext() ) {
            return false;
        }
        autoLog = cursor.getString( 2 ).equals("login") && cursor.getInt( 3 ) > 0;
        access.close();
        UserRequests ur = new UserRequests(context);
        Informations.user = ur.getUser( cursor.getString( 1 ) );
        return autoLog;
    }

    private DAOAccess access;
    private Context context;
    private final String name_table = "history";
}
