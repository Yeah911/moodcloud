package com.moodcloud.moodcloud.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.Request.UserRequests;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.util.Utils;

import cz.msebera.android.httpclient.Header;


public class SignIn extends AppCompatActivity {
    private UserRequests usrReq;
    AsyncResponse ar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        usrReq = new UserRequests( getApplicationContext() );
        final EditText loginField = (EditText) findViewById(R.id.login);
        final EditText emailField = (EditText) findViewById(R.id.email);
        final EditText pwdField = (EditText) findViewById(R.id.pwd);
        final EditText confirmPwdField = (EditText) findViewById(R.id.confirm_pwd);
        Button signin = (Button) findViewById(R.id.singin);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginField.getText().toString();
                String email = emailField.getText().toString();
                String pwd = pwdField.getText().toString();
                String confirmPwd = confirmPwdField.getText().toString();
                if( login.isEmpty() ) {
                    Toast.makeText(getApplicationContext(), "error login empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if( email.isEmpty() ) {
                    Toast.makeText(getApplicationContext(), "error email empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if( !Utils.isCorrectEmail(email) ) {
                    Toast.makeText(getApplicationContext(), "error email incorrect", Toast.LENGTH_LONG).show();
                    return;
                }
                if( pwd.isEmpty() || !pwd.equals(confirmPwd) ) {
                    // TODO : show message error
                    Toast.makeText(getApplicationContext(), "error passwords and confirm password are differents or empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if( usrReq.getUserByLogin(login) != null ) {
                    Toast.makeText(getApplicationContext(), "error this login exist on an other account", Toast.LENGTH_LONG).show();
                    return;
                }

                if( usrReq.getUserByEmail(email) != null ) {
                    Toast.makeText(getApplicationContext(), "error this email exist on an other account", Toast.LENGTH_LONG).show();
                    return;
                }

                sendRequestSignIn(login,email,pwd);
            }
        });

    }
    private void sendRequestSignIn(final String login, final String email, final String pwd){

        RequestParams rp = new RequestParams();
        rp.put("userName",login);
        rp.put("email", email);
        final String pwdHashed = Utils.encryptMD5(pwd,false);
        if(pwdHashed== null) return;
        rp.put("pwd",pwdHashed);
        AsyncResponse.sendRequestPost(Connection.URLRedirection.register, rp, new CallbackResponse() {
            @Override
            public void onSuccess(boolean registered) {

                if( registered ) {
                    findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    usrReq.register( new User(login, pwd, email) );
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "error inscription ", Toast.LENGTH_LONG).show();
                }
            }

            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Connection.URLRedirection redirect, Throwable error) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                if(statusCode == 0)
                    Toast.makeText(getApplicationContext(), "Problème de connexion au serveur", Toast.LENGTH_LONG).show();
            }
        });
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }
}
