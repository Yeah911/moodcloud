package com.moodcloud.moodcloud.persistance.Request;


public class SqlRequest {

    // tables creations
    public static final String create_user_tables = "CREATE TABLE User (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT, email TEXT);";
    public static final String create_history_tables = "CREATE TABLE history (id INTEGER PRIMARY KEY AUTOINCREMENT, id_user INTEGER , action TEXT, date DATETIME, auto_login BOOLEAN, FOREIGN KEY(id_user) REFERENCES User(history_id_user));";
    public static final String create_mood_tables = "CREATE TABLE mood (id INTEGER PRIMARY KEY AUTOINCREMENT, id_mood INTEGER, id_user INTEGER, mark INTEGER, comment TEXT, date DATETIME, FOREIGN KEY(id_user) REFERENCES User(mood_id_user));";
    public static final String create_mood_unsend = "CREATE TABLE mood_unsend (id INTEGER PRIMARY KEY AUTOINCREMENT, id_mood INTEGER, FOREIGN KEY(id_mood) REFERENCES mood(id_mood_unsend));";
    public static final String create_mood_friends = "CREATE TABLE friend (id INTEGER PRIMARY KEY AUTOINCREMENT, id_user_ask INTEGER, id_user INTEGER , date DATETIME, friend BOOLEAN, blocked BOOLEAN, FOREIGN KEY(id_user) REFERENCES User(friend_id_user_asker), FOREIGN KEY(id_user) REFERENCES User(friend_id_user_asked));";
    public static final String create_mood_discution = "CREATE TABLE discution (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, date DATETIME, closed BOOLEAN);";
    public static final String create_mood_discution_list = "CREATE TABLE discution_list (id INTEGER PRIMARY KEY AUTOINCREMENT, id_mood INTEGER, id_discution INTEGER , id_user INTEGER, FOREIGN KEY(id_mood) REFERENCES discution(discutionList_id_mood), FOREIGN KEY(id_discution) REFERENCES discution(discutionList_id_discution), FOREIGN KEY(id_user) REFERENCES User(discutionList_id_user));";
    public static final String create_mood_message = "CREATE TABLE messages (id INTEGER PRIMARY KEY AUTOINCREMENT, id_discution INTEGER FOREIGN KEY, id_user INTEGER FOREIGN KEY, message TEXT, FOREIGN KEY(id_discution) REFERENCES discution(message_id_discution), FOREIGN KEY(id_user) REFERENCES User(message_id_user));";

    // selections
    // history
    public static  final String select_last_histoy = "select id, id_user, action, auto_login from history ORDER BY id desc";
    // user
    public static  final String select_user = "select * from User Where ?";
    public static  final String select_user_by_id = "select * from User Where id = ?";
    public static  final String select_user_by_login = "select * from User Where login = ?";
    public static  final String select_user_by_email = "select * from User Where email = ?";
    public static  final String select_existing_user = "select id, login, password, email from User Where email = ? AND password = ?";
    // mood
    public static  final String select_mood = "select * FROM mood Where id = ?";
    public static  final String select_all_mood = "select * FROM mood Where id_user = ?";
    // moode unsend
    public static  final String select_mood_unsend = "select id, id_mood FROM mood_unsend Where id_mood = ?";
    // friends
    public static  final String select_all_friends_asked_didnt_accept = "select id, login, email FROM User Where id_user_ask = ? AND friend=false";
    public static  final String select_all_friends = "select id, login, email FROM User Where (id_user_ask = ? OR id_user = ?) AND friend=true";
    public static  final String select_all_friends_asked = "select id, login, email FROM User Where id_user_ask = ? AND friend=false";
    public static  final String select_all_asked_friends = "select id, id_user_ask, id_user, date , friend , blocked FROM friend Where (id_user_ask = ? OR id_user = ?) AND (id_user_ask = ? OR id_user = ?)";
    // discution
    public static  final String select_discution = "select id, name, date, closed FROM discution WHERE closed=false";
    public static  final String select_discution_by_name = "select id, name, closed FROM discution WHERE CONTAINS(name, ?) AND closed=false"; //  name LIKE '%?%';
    public static  final String select_user_in_discution = "select id, id_discution, id_user FROM discution_list WHERE id_discution = ?";
    public static  final String select_messages_in_discution = "select id, id_discution, id_user, message FROM messages WHERE id_discution = ?";

    //tracking
    public static  final String select_mood_rate_range = "select mark,comment,id FROM mood where strftime('%s', date) BETWEEN strftime('%s', ?) AND strftime('%s', ?) AND id_user = ? order by id DESC limit 20";
    public static  final String select_mood_rate = "select mark,comment, id FROM mood WHERE id_user = ? ORDER BY id DESC limit 20  ";

    public static  final String select_seven_last_days = "select  date from mood WHERE id_user = ? and date > (SELECT DATETIME('now', '-7 day'))  order by date asc ";
    public static final String select_moyenne_for_day = "select  sum(mark)/count(*) from mood where strftime('%s', date) BETWEEN strftime('%s', ?) AND strftime('%s', ?)";

    public static  final String seelct_mood_count = "select  count(*) from mood  WHERE id_user = ?";
    public static  final String select_mood_average = "sum(mark)/count(*) from mood WHERE id_user = ?";
    //Commentaires

}
