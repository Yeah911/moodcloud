package com.moodcloud.moodcloud.Activity.Actuality;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.Activity.ChatMessage;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.Conversation;
import com.moodcloud.moodcloud.model.ListModel;
import com.moodcloud.moodcloud.model.Message;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Emerich on 28/06/2016.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> implements View.OnClickListener {

    private List<ContactInfo> contactList;
    private final String baseList = "Contacts";
    protected ContactInfo current;
    private List<User> users = new ArrayList<>();
    private List<String> elements = new ArrayList<>();
    private List<String> elementsListContact = new ArrayList<>();
    private ListModel currentList;
    private ListModel contactsList = null;
    private boolean found = false;
    Context ctx;
    public ContactAdapter(Context context) {
        this.contactList = new ArrayList<>();
        ctx = context;
        setElementsContactList();
    }

    private void setElementsContactList() {

        AsyncResponse.sendRequestGet(Connection.URLRedirection.retreiveNewsFeed, new CallbackResponse() {
            @Override
            public void onSuccess(JSONArray json) {
                contactList.addAll( Parser.parseMoodShared(json) );
                Collections.sort(contactList, getComparator());
                notifyDataSetChanged();
            }
        });
    }

    private Comparator<ContactInfo> getComparator() {
        return new Comparator<ContactInfo>() {
            @Override
            public int compare(ContactInfo lhs, ContactInfo rhs) {

                return lhs.mood.getDateLong()  < rhs.mood.getDateLong() ? 1 :
                        lhs.mood.getDateLong() > rhs.mood.getDateLong() ? -1 : 0 ;
            }
        };
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        final ContactInfo ci = contactList.get(i);
        contactViewHolder.vNote.setText("Note : " + ci.note);
        contactViewHolder.vCom.setText("Commentaire : " + ci.com);
        contactViewHolder.vTitle.setText(ci.name );
        contactViewHolder.current = ci;

        contactViewHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncResponse.sendRequestGet(Connection.URLRedirection.getFriends, new CallbackResponse() {

                    @Override
                    public void onSuccess(JSONArray json) {
                        users = Parser.parseUsers(json);
                        startChat(ci);
                    }
                });
            }
        });


    }

    private void startChat(ContactInfo ci){

        if( contactsList == null ) {
            contactsList = new ListModel("0", baseList, users);
        }

        getConversation(ci);


    }

    private void getConversation(final ContactInfo ci) {
        RequestParams rp = new RequestParams();
        found = false;
        rp.put("moodId", ci.getMood().getGuid());
        AsyncResponse.sendRequestGet(Connection.URLRedirection.retrieveMoodConversations, rp, new CallbackResponse() {
            @Override
            public void onSuccess(JSONObject json) {
                List<Conversation> conversations = Parser.parseConversation( json );
                if( conversations.isEmpty() ) {
                    createConversation(ci);
                    return;
                }
                retreiveConversation(ci, conversations.get(0).getId());
                super.onSuccess(json);
            }
        });
    }


    private void retreiveConversation(final ContactInfo ci, String convId) {
        RequestParams rp = new RequestParams();
        rp.put("conversationId", convId);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.retreiveConversation, rp, new CallbackResponse() {

            @Override
            public void onSuccess(JSONObject json) {
                Message message = Parser.parseMessagesConversation(json);
                if( message.getTo().getEmail().equals( ci.getUser().getEmail() ) ) {
                    found = true;
                    startConversation(message);
                } else {
                    found = false;
                }
            }

        });
    }

    private void createConversation(ContactInfo ci) {
        RequestParams rp = new RequestParams();
        rp.put("moodId", ci.getMood().getGuid());
        AsyncResponse.sendRequestGet(Connection.URLRedirection.createMoodConversations, rp, new CallbackResponse() {
            @Override
            public void onSuccess(JSONObject json) {
                Message message = Parser.parseMessagesConversation(json);
                startConversation(message);
            }
        });
    }

    private void startConversation(Message message) {
        if(ctx instanceof Actus){
            ((Actus)ctx).startConversation(message,true);
        }
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.actu_card_view, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    @Override
    public void onClick(View v) {
        
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        protected TextView vNote;
        protected TextView vCom;
        protected TextView vTitle;
        protected CardView card;
        protected ContactInfo current;
        protected Context context;

        public ContactViewHolder(View v, Context c) {
            this(v);
            context = c;
        }

        public ContactViewHolder(View v) {
            super(v);
            vNote =  (TextView) v.findViewById(R.id.txtNote);
            vCom = (TextView)  v.findViewById(R.id.txtComent);
            vTitle = (TextView) v.findViewById(R.id.title);
            card = (CardView) v.findViewById(R.id.card_view);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
