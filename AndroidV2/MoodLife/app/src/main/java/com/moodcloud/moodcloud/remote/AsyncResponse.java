package com.moodcloud.moodcloud.remote;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Yehouda on 06/06/2016.
 */
public class AsyncResponse {
    public int isRunning;

    public void register(String username, String email, String password) {
        isRunning = 0;
        RequestParams rp = new RequestParams();
        rp.put("userName",username);
        rp.put("email", email);
        String pwdHashed = Utils.encryptMD5(password);
        if(pwdHashed== null) return;
        rp.put("pwd",pwdHashed);
        sendRequestPost(Connection.URLRedirection.register, rp, new CallbackResponse() {

            @Override
            public void onSuccess(boolean registered) {
                if( registered ) {
                    isRunning =1;
                } else {
                    isRunning = -1;
                }
            }

        });
    }

    public static void sendRequestGet(final Connection.URLRedirection redirect, final CallbackResponse callback) {
        sendRequestGet(redirect, null, null, callback);
    }

    public static void sendRequestGet(final Connection.URLRedirection redirect, RequestParams params, final CallbackResponse callback) {
        sendRequestGet(redirect, null, params, callback);
    }

    public static void sendRequestGet(final Connection.URLRedirection redirect, Map<String, String> urlParameters, RequestParams params, final CallbackResponse callback) {
        StringBuilder sb = new StringBuilder();
        sb.append(redirect.getUrl()).append(".json.cmd.ashx");
        if( urlParameters != null && !urlParameters.isEmpty() ) {
            sb.append("?");
            int i = 0;
            for( Map.Entry<String, String> entry : urlParameters.entrySet() ) {
                if ( i++ > 0 ) {
                    sb.append("&");
                }
                String key = entry.getKey();
                String val = entry.getValue();
                sb.append(key).append("=").append(val);
            }
        }
        Connection.get(sb.toString(), params, getGenericAsyncHttp(redirect, callback));
    }

    public static void sendRequestPost(final Connection.URLRedirection redirect, RequestParams params, final CallbackResponse callback) {
        StringBuilder sb = new StringBuilder();
        sb.append(redirect.getUrl()).append(".json.cmd.ashx");
        Connection.post(sb.toString(), params, getGenericAsyncHttp(redirect, callback));
    }

    private static AsyncHttpResponseHandler getGenericAsyncHttp(final Connection.URLRedirection redirect, final CallbackResponse callback) {
        return new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    String response = Utils.parseResponseFormattage(new String(responseBody, "UTF-8"), "(", ")");
                    response = Utils.formatDateDeclaration(response, "new Date(", ")");
                    Connection.exchangeAspectizeCookie(headers);
                    if ( !isEmptyResponse(response) ) {

                        char c = response.charAt(0);
                        if( c == '\'' ) {
                            callback.onSuccess( Utils.parseResponseFormattage(response, "'") );
                        } else if( c == '{' ) {
                            callback.onSuccess(new JSONObject(response));
                        } else if( c == '[' ) {
                            callback.onSuccess(new JSONArray(response));
                        } else if(Character.isLetter(c) ) {
                            if( response.equals("true") || response.equals("false") ) {
                                callback.onSuccess( (boolean) Boolean.valueOf( response ) );
                            } else {
                                callback.onFailure(statusCode, headers, responseBody, redirect, new Exception("Unknown return type."));
                            }
                        }
                    } else {
                        callback.onSuccess();
                    }
                } catch (Exception e) {
                    callback.onFailure(e);
                }
            }

            private boolean isEmptyResponse(String response) {
                return response.isEmpty() || response.equals("{}") || response.equals("[]");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onFailure(statusCode, headers, responseBody, redirect, error);
            }
        };
    }
}
