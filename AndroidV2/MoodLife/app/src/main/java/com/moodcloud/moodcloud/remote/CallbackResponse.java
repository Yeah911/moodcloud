package com.moodcloud.moodcloud.remote;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Yehouda on 07/06/2016.
 */
public abstract class CallbackResponse {

    Logger LOGGER = Logger.getLogger(CallbackResponse.class.getName());

    public CallbackResponse() {
    }

    public CallbackResponse(AppCompatActivity activity) {
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    public CallbackResponse(Context context) {
        this.context = context;
    }


    public void onConnect(Object message) {
    }

    public void onSuccess() {
    }

    public void onSuccess(JSONObject json) {
    }

    public void onSuccess(JSONArray json) {
    }

    public void onSuccess(Object message) {
    }

    public void onSuccess(String message) {
    }

    public void onSuccess(boolean element) {
    }

    public void onFailure(String message) {
        LOGGER.log(Level.SEVERE, message);
        if( context != null )
            Utils.toast(context, message);
    }

    public void onFailure() {
        onFailure("error");
    }

    public void onFailure(Exception e) {
        onFailure("error");
    }

    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        try {
            onFailure( Utils.byteArrayToString(responseBody) );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Connection.URLRedirection redirect, Throwable error) {
        try {
            onFailure( Utils.byteArrayToString(responseBody) );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    protected Context context;
    protected AppCompatActivity activity;
}
