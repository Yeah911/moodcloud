package com.moodcloud.moodcloud.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Message implements Serializable {

    public Message() {
        messages = new ArrayList<>();
    }

    public Message(String id, User from, User to, String content, Conversation conversation) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.conversation = conversation;
        messages = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public Mood getMood() {
        return mood;
    }

    public void setMood(Mood mood) {
        this.mood = mood;
    }

    public List<MoodMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<MoodMessage> messages) {
        this.messages = messages;
    }

    public void addMessage(MoodMessage message) {
        if( messages == null )
            messages = new ArrayList<>();
        this.messages.add(message);
    }

    private String id;
    private User from;
    private User to;
    private List<MoodMessage> messages;
    private Conversation conversation;
    private Mood mood;
}
