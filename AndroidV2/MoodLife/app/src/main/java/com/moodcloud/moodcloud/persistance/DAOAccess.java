package com.moodcloud.moodcloud.persistance;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAOAccess {

    public DAOAccess(Context context, String name, String create) {
        this.database = new Database(context, name, create, null, VERSION);
        this.name = name;
    }

    public DAOAccess(Context context, String name, String create, int version) {
        this.database = new Database(context, name, create, null, version);
        this.name = name;
    }

    public SQLiteDatabase open() {
        sqliteDb = database.getWritableDatabase();
        return sqliteDb;
    }

    public void close() {
        if(sqliteDb.isOpen())
            sqliteDb.close();
    }

    public void add(String name, ContentValues values) {
        sqliteDb.insert(name, null, values);
    }

    public void modify(String name, String conditions, String[] newValues, ContentValues values) {
        sqliteDb.update(name, values, conditions, newValues);
        // conditions = "id = ?"
        // newValues = ["5"]
    }

    public Cursor get(String select, String [] valuesConditions) {
        Cursor cursor = sqliteDb.rawQuery(select, valuesConditions);
        if( cursor != null) {
            return cursor;
        }
        return null;
    }

    public void delete(String name, String id){
        sqliteDb.delete( name, "id = ?", new String[]{id} );
    }

    public static int VERSION = 1;
    private String name;
    private SQLiteDatabase sqliteDb;
    private Database database;
}
