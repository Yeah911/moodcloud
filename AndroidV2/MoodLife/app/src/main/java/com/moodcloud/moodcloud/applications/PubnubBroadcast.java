package com.moodcloud.moodcloud.applications;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.moodcloud.moodcloud.Activity.Actuality.Actus;
import com.moodcloud.moodcloud.Activity.Contacts;
import com.moodcloud.moodcloud.Activity.FriendRequest;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.NotificationCreator;
import com.moodcloud.moodcloud.util.Utils;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Yehouda on 09/06/2016.
 */
public class PubnubBroadcast extends Application {

    private static String title;
    private static String text;

    private static Pubnub pubnub = new Pubnub(Informations.publishKey, Informations.subscribeKey);

    public static void publish(String channel, String message, final CallbackResponse response) {
        pubnub.publish(channel, message, getGenericCallback(CallbackType.publish, response));
    }

    public static void subscribe(String channel, final CallbackResponse response) throws PubnubException {
        pubnub.subscribe(channel, getGenericCallback(CallbackType.subscribe, response));
    }

    public static void unsubscribe(String channel) throws PubnubException {
        pubnub.unsubscribe(channel);
    }

    public static void history(String channel, int numberHistory, final CallbackResponse response) {
        pubnub.history(channel, numberHistory, getGenericCallback(CallbackType.history, response));
    }


    private static Callback getGenericCallback(final CallbackType type, final CallbackResponse response) {

        return new Callback() {
            @Override
            public void successCallback(String channel, Object message) {
                        String mess = Utils.formatDateDeclaration(message.toString(), "new Date(", ")");
                        if( mess.isEmpty() ) return;
//                messageType = 0; // moodType
                        char c = mess.charAt(0);
                        try {
                            if( c == '{' ) {
                                parseJSONObject( new JSONObject( mess ), response );
                            } else if( c == '[' ) {
                                response.onSuccess( new JSONArray( mess ) );
                            } else {
                                response.onSuccess(mess);
                            }
                        } catch (JSONException e) {
                            response.onFailure(e);
                        }

            }

            @Override
            public void successCallback(String channel, Object message, String timetoken) {
                response.onSuccess(message);
                super.successCallback(channel, message, timetoken);
            }

            @Override
            public void successCallbackV2(String channel, Object message, JSONObject envelope) {
                response.onSuccess(message);
                super.successCallbackV2(channel, message, envelope);
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
                response.onFailure(error.getErrorString());
            }

            @Override
            public void connectCallback(String channel, Object message) {
                response.onConnect(message);
            }

            @Override
            public void reconnectCallback(String channel, Object message) {
                super.reconnectCallback(channel, message);
            }

            @Override
            public void disconnectCallback(String channel, Object message) {
                super.disconnectCallback(channel, message);
            }
        };
    }


    private static void parseJSONObject( JSONObject json, CallbackResponse response ) {
        try {
            String type = json.getString("messageType");
            JSONArray data;
            switch ( type ) {
                case "SubscribeToChannels":
                    JSONArray datas = json.getJSONArray("data");
                    for(int i = 0; i < datas.length(); i++) {
                        data = datas.getJSONArray(i);
                        for (int j = 0; j < data.length(); j++) {
                            JSONObject obj = data.getJSONObject(j);
                            String id = Utils.formatChannel( obj.getString("Id") );
                            String name = obj.getString("Name");
                            Informations.broadcastLists.put(id, name);
                            PubnubBroadcast.subscribe(id, new CallbackResponse() {});
                        }
                    }
                    break;
                case "SubscribeToChannel":
                    data = json.getJSONArray("data");
                    for(int i = 0; i < data.length(); i++) {
                        String id = Utils.formatChannel( data.get(i).toString() );
                        Informations.broadcastLists.put(id, "BroadcastPublic");
                        PubnubBroadcast.subscribe(id, new CallbackResponse() {});
                    }
                    title = "nouveau mood";
                    text = "";
                    notify(Actus.class, title, text);
                    break;
                case "FriendRequest":
                    data = json.getJSONArray("data");
                    for(int i = 0; i < data.length(); i++) {
                        JSONObject obj = data.getJSONObject(i);
                        User requester = Parser.parseAnUser( obj.getJSONObject("data").getJSONObject("requester") );
                        title = "Une demande d'ajout d'ami";
                        text = requester.getLogin() + " vous demande en ami";
                        notify(FriendRequest.class, title, text);
                    }
                    break;
                case "FriendRequestAccepted":
                    data = json.getJSONArray("data");
                    for(int i = 0; i < data.length(); i++) {
                        JSONObject obj = data.getJSONObject(i);
                        User requester = Parser.parseAnUser( obj.getJSONObject("data").getJSONObject("requested") );
                        title = "Un ami a accepter votre demande";
                        text = requester.getLogin() + " a accepter votre demade";
                        notify(Contacts.class, title, text);
                    }
                    break;
                default:
                    response.onSuccess(json);
                    break;
            }

        } catch (JSONException e) {
            response.onSuccess( json );
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }
    public static void notify(Intent intent,  String title, String text) {
        if( activity == null || intent == null ) return;
        NotificationCreator nc = new NotificationCreator(activity, intent);
        nc.create(R.mipmap.ic_launcher, title, text);
    }

    public static void notify(String title, String text) {
        if( activity == null || intent == null ) return;
        NotificationCreator nc = new NotificationCreator(activity, intent);
        nc.create(R.mipmap.ic_launcher, title, text);
    }

    public static void notify(AppCompatActivity activity, Intent intent, String title, String text) {
        if( activity == null || intent == null ) return;
        NotificationCreator nc = new NotificationCreator(activity, intent);
        nc.create(R.mipmap.ic_launcher, title, text);
    }

    public static void notify(Class<?> cls, String title, String text) {
        if( activity == null || intent == null ) return;
        Intent intent = new Intent(activity, cls);
        intent.putExtra("notification", true);
        NotificationCreator nc = new NotificationCreator(activity, intent);
        nc.create(R.mipmap.ic_launcher, title, text);
    }

    public static void subscribePrivateChannel() {
        subscribePrivateChannel(null, null);
    }

    public static void subscribePrivateChannel( AppCompatActivity activity, Intent intent ) {
        PubnubBroadcast.activity = activity;
        PubnubBroadcast.intent = intent;
        AsyncResponse.sendRequestGet(Connection.URLRedirection.getPrivateChannel, new CallbackResponse() {
            @Override
            public void onSuccess(String message) {
                Informations.channel = message;
                try {
                    subscribe(Informations.channel, new CallbackResponse() {
                        @Override
                        public void onConnect(Object message) {
                            super.onConnect(message);
                            getChannelToSubscribe(this);
                        }

                        @Override
                        public void onSuccess() {
                            super.onSuccess();
                            launchNotify();
                        }

                        @Override
                        public void onSuccess(JSONObject json) {
                            super.onSuccess(json);
                            launchNotify();
                        }

                        @Override
                        public void onSuccess(JSONArray json) {
                            super.onSuccess(json);
                            launchNotify();
                        }

                        @Override
                        public void onSuccess(Object message) {
                            super.onSuccess(message);
                            launchNotify();
                        }

                        @Override
                        public void onSuccess(String message) {
                            super.onSuccess(message);
                            launchNotify();
                        }

                        @Override
                        public void onSuccess(boolean element) {
                            super.onSuccess(element);
                            launchNotify();
                        }

                        private void launchNotify() {
                            //PubnubBroadcast.notify(title, text);
                        }

                        private void parseJson( JSONObject json ) {

                        }

                        private void parseJson( JSONArray json ) {

                        }

                        private String title;
                        private String text;
                        private Intent intent;
                    });
                } catch (PubnubException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void getChannelToSubscribe(final CallbackResponse response) {
        AsyncResponse.sendRequestGet(Connection.URLRedirection.subscribeChannel, new CallbackResponse() {

            @Override
            public void onSuccess(JSONObject json) {
                parseJSONObject(json, response);
            }
        });
    }

    private static AppCompatActivity activity;
    private static Intent intent;
    private static Class<?> cls;

    public static enum CallbackType {
        subscribe,
        publish,
        unsubscribe,
        history
    }

}
