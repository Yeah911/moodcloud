package com.moodcloud.moodcloud.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.ListModel;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Contacts extends AppCompatActivity implements View.OnClickListener{

    private ListView list;
    private ArrayAdapter<String> adapter;
    private List<User> users = new ArrayList<>();
    private List<String> elements = new ArrayList<>();
    private List<String> elementsListContact = new ArrayList<>();
    private ListModel currentList;
    private ListModel contactsList = null;
    private final String baseList = "Contacts";
    private ArrayAdapter<String> adapterListContact;
    private List<ListModel> listFriends = new ArrayList<>();
    private AlertDialog dialog;
    private TextView titleList;
    private Menu menu;
    private boolean forceLists = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        if( getIntent().hasExtra("notification") ) {
            setListFriends(true);
        }

        list = (ListView) findViewById(R.id.contactListView);
        titleList = (TextView) findViewById(R.id.title_list);
        Button btn = (Button) findViewById(R.id.addContact);
        btn.setOnClickListener(this);

        Button btn2 = (Button) findViewById(R.id.manageList);
        btn2.setOnClickListener(this);

        adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.simple_list, elements);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                addToListOrDelete(position, currentList != null && !currentList.name.equals(baseList));
            }
        });

        AsyncResponse.sendRequestGet(Connection.URLRedirection.getFriends, new CallbackResponse() {

            @Override
            public void onSuccess(JSONArray json) {
                users = Parser.parseUsers(json);
                setElements(users);
                if( contactsList == null ) {
                    contactsList = new ListModel("0", baseList, users);
                }
                currentList = contactsList;
                setListFriends();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch( v.getId() ) {
            case R.id.addContact :
                Intent i = new Intent(this, UserAdd.class);
                startActivity(i);
                break;

            case R.id.manageList :
                showGestureListFriends();
                break;

        }
    }

    private void showGestureListFriends() {

        if( listFriends.size() > 1 && ! forceLists ) {
            gestureListFriends();
            return;
        }
        forceLists = false;
        AsyncResponse.sendRequestGet(Connection.URLRedirection.broadcastLists, new CallbackResponse(this) {
            @Override
            public void onSuccess(JSONArray json) {
                listFriends.clear();
                listFriends.add(contactsList);
                listFriends.addAll(Parser.parseListFriends(json));
                gestureListFriends();
            }

            @Override
            public void onSuccess() {
                Utils.toast(context, "Vous n'avez pas créer de liste d'amis");
                if( listFriends.isEmpty() ) {
                    listFriends.add(contactsList);
                }
                gestureListFriends();
            }
        });
    }

    private void gestureListFriends() {
        LayoutInflater li = LayoutInflater.from(getBaseContext());
        final View customView = li.inflate(R.layout.dialog_gesture_list, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(Contacts.this);
        this.dialog = builder.create();
        final EditText input = (EditText) customView.findViewById(R.id.inputListDialog);
        final ListView list = (ListView) customView.findViewById(R.id.listViewDialog);

        elementsListContact.clear();
        for( ListModel ml : listFriends) {
            elementsListContact.add(ml.name);
        }

        adapterListContact = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, elementsListContact);
        list.setAdapter(adapterListContact);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for( ListModel lm : listFriends ) {
                    if( lm.name.equals(elementsListContact.get(position)) ) {
                        currentList = lm;
                    }
                }
                List<User> usersInList = currentList.users;
                elements.clear();
                if( currentList == null ) {
                    Utils.toast(getApplicationContext(), "list d'amis vide");
//                    return;
                } else {
                    for( User usr : usersInList ) {
                        elements.add( usr.getLogin() );
                    }
                }
                updateMenu();
                titleList.setText(currentList.name);
                adapter.notifyDataSetChanged();
                Contacts.this.dialog.dismiss();
            }
        });

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        dialog.setView(customView);
        dialog.setTitle("gestion liste");
        dialog.setMessage("nom de list");
        dialog.setCancelable(true);
        dialog.show();

        Button create = (Button) customView.findViewById(R.id.create_list);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( input.getText().toString().isEmpty() ) {
                    Utils.toast(getApplicationContext(), "nom de list vide");
                    return;
                }
                createList(input.getText().toString());
            }
        });
    }

    private void addToListOrDelete(final int pos, boolean isFromList) {
        final LayoutInflater li = LayoutInflater.from(getBaseContext());
        View customView = li.inflate(R.layout.dialog_add_list_or_delete, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(Contacts.this);
        this.dialog = builder.create();
        final ListView list = (ListView) customView.findViewById(R.id.listFriendsToAdd);
        final Button remove = (Button)   customView.findViewById(R.id.remove);

        if( isFromList ) {
            list.setVisibility(View.GONE);
            final TextView tv = (TextView) customView.findViewById(R.id.textView8);
            tv.setVisibility(View.GONE);

            final TextView tv2 = (TextView) customView.findViewById(R.id.textView7);
            tv2.setText("Supprimer de la list " + currentList + "?");
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeFriendFromList(currentList.users.get(pos));
                }
            });

            dialog.setView(customView);
            dialog.setTitle("ajouter a une liste ou supprimer");
            dialog.setCancelable(true);
            dialog.show();
            return;
        }

        elementsListContact.clear();
        elementsListContact.clear();
        for( ListModel ml : listFriends) {
            if( ! ml.name.equals(baseList) )
                elementsListContact.add(ml.name);
        }

        adapterListContact = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, elementsListContact);
        list.setAdapter(adapterListContact);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListModel mod = getListFromFriends(elementsListContact.get(position));
                User usr = users.get(pos);
                if( mod.users.contains(usr) ) {
                    Utils.toast(getApplicationContext(), "cet utilisateur existe deja dans cette liste");
                    return;
                }
                mod.users.add(usr);
                addFriendInList(mod.id, usr.getId());
                Contacts.this.dialog.dismiss();
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFriend(users.get(pos));
            }
        });

        dialog.setView(customView);
        dialog.setTitle("ajouter a une liste ou supprimer");
        dialog.setCancelable(true);
        dialog.show();
    }

    private void removeFriend(final User friend ) {
        RequestParams rp = new RequestParams();
        rp.put("friendID", friend.getId());
        AsyncResponse.sendRequestGet(Connection.URLRedirection.removeFriend, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                super.onSuccess();
                Utils.toast(getApplicationContext(), "amis supprimé");
                elements.remove(friend.getLogin());
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
    }

    public void removeFriendFromList( final User usr ) {
        String userId = "(['" + usr.getId() + "'])";
        RequestParams rp = new RequestParams();
        rp.put("listId", currentList.id);
        rp.put("users", userId);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.removeFriendBroadcastLists, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                super.onSuccess();
                Utils.toast(getApplicationContext(), "amis enlevé de la list");
                elements.remove( usr.getLogin() );
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
    }

    private void addFriendInList(String listid, String usrId) {

        RequestParams rp = new RequestParams();
        rp.put("listId", listid);
        rp.put("users", "(['" + usrId + "'])");
        AsyncResponse.sendRequestGet(Connection.URLRedirection.addFriendBroadcastLists, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                super.onSuccess();
            }
        });
    }

    private void createList(String name) {

        RequestParams rp = new RequestParams();
        rp.put("name", name);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.createBroadcastLists, rp, new CallbackResponse() {
            @Override
            public void onSuccess(JSONObject json) {
                super.onSuccess(json);
                String listName = null;
                try {
                    listName = json.getString("Name");
                    String id = json.getString("Id");
                    listFriends.add(new ListModel( id, listName, new ArrayList<User>()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if( listName == null ) return;
                elementsListContact.add(listName);
                adapterListContact.notifyDataSetChanged();
            }

        });
    }

    private void setListFriends() {
        setListFriends(false);
    }

    private void setListFriends(boolean force) {

        if( listFriends.size() > 1 && !force ) {
            return;
        }

        AsyncResponse.sendRequestGet(Connection.URLRedirection.broadcastLists, new CallbackResponse(this) {
            @Override
            public void onSuccess(JSONArray json) {
                listFriends.clear();
                listFriends.add(contactsList);
                listFriends.addAll(Parser.parseListFriends(json));
            }
        });
    }

    private void setElements(List<User> us) {
        if( !elements.isEmpty() )
            elements.clear();
        for( User u : users )
            elements.add(u.getLogin());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_menu, menu);
        this.menu = menu;
        updateMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
//            case R.id.modify_list:
//                break;
            case R.id.delete_list:
                deleteList();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteList() {
        RequestParams rp = new RequestParams();
        rp.put("listId", currentList.id);
        AsyncResponse.sendRequestGet(Connection.URLRedirection.deleteBroadCastList, rp, new CallbackResponse() {
            @Override
            public void onSuccess() {
                super.onSuccess();
                forceLists = true;
                listFriends.remove(currentList);
                currentList = getListFromFriends(baseList);
                updateMenu();
                elements.clear();
                for( User usr : users ) {
                    elements.add( usr.getLogin() );
                }
                titleList.setText(currentList.name);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private ListModel getListFromFriends(String name) {
        ListModel result = null;
        for( ListModel m : listFriends ) {
            if (name.equals(m.name)) {
                result = m;
                break;
            }
        }
        return result;
    }

    private void updateMenu() {
        if(currentList == null || currentList.name.equals(baseList)) {
            for( int i = 0; i < menu.size(); i++ ) {
                menu.getItem(i).setVisible(false);
            }
            return ;
        }

        for( int i = 0; i < menu.size(); i++ ) {
            menu.getItem(i).setVisible(true);
        }
    }
}
