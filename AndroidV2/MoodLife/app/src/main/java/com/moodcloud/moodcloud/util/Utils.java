package com.moodcloud.moodcloud.util;

import android.content.Context;
import android.location.Location;
import android.util.Base64;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.Exchanger;

public class Utils {

    private static DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
    private static DateFormat timeFormat = new SimpleDateFormat("K:mma");

    public static String getCurrentTime() {
        Date today = Calendar.getInstance().getTime();
        return timeFormat.format(today);
    }

    public static String getCurrentDateChat() {
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurrentDateString() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRENCH);
        Calendar cal = Calendar.getInstance();
        return dateFormatter.format(cal.getTime());
    }

    public static Calendar getCurrentDateCalendar(String dt) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRENCH);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(dateFormatter.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static String getCurrentDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return df.format(date);
    }

    public static Date getDateFromUnix(String s) {
        if( s == null || s.isEmpty() ) return new Date();
        try {
            Long temps = Long.valueOf(s);
            temps = temps * 1000L;
            s = String.valueOf(temps);
            return new Date(temps);
        }
        catch (Exception e){
            e.printStackTrace();
        }
       /* SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        return null;
    }

    public static Date getDate(String s) {
        if( s == null || s.isEmpty() ) return new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getCurrentDateUTC() {
        return  Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
    }

    public static Date getDateUTC(String s) {
        TimeZone tz = SimpleTimeZone.getTimeZone("Europe/Paris");
        String name = tz.getDisplayName();
        String id = tz.getID();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss z");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        Date utcDate = null;
        try {
            utcDate = sdf.parse( s );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return utcDate;
    }
//    private static final DateTimeZone PARIS = DateTimeZone.forID("Europe/Paris");

    public static Date getLocalDate(String s) {
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        Date date = null;
        try {
            date = sdf.parse( s );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static String formatChannel(String chan) {
        StringBuilder sb = new StringBuilder(chan);
        sb.insert(8, '-');
        sb.insert(13, '-');
        sb.insert(18, '-');
        sb.insert(23, '-');
        return sb.toString();
    }

    public static Date getDate(long s) {
        return new Date(s);
    }

    public static boolean isCorrectEmail(String email) {
        return email.contains("@") && email.contains(".");
    }

    public static String encryptMD5(String something) {
        return encryptMD5(something, true);
    }

    public static String encryptMD5(String something, boolean replace) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(something.getBytes());
            String hash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
            if(replace)
                hash = hash.replaceAll("\\+", "%20");
            return hash.substring( 0, hash.indexOf("=") );
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String byteArrayToString(byte[] bytes) throws UnsupportedEncodingException {
        if( bytes == null || bytes.length == 0 )
            return "";
        return new String(bytes, "UTF-8");
    }

    public static String parseResponseFormattage(String response, String start) {
        return parseResponseFormattage(response, start, start);
    }

    public static String parseResponseFormattage(String response, String start, String end) {
        if( response.startsWith(start) ) {
            response = response.substring(start.length());
        }
        if ( response.endsWith(end) ) {
            response = response.substring(0, response.length() - end.length());
        }
        return response;
    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String formatDateDeclaration(String response, String startWith, String endWith) {

        String declareDate = "new Date("
                , endDeclareDate = ")";

        while (response.contains(declareDate)){
            int start = response.indexOf(declareDate),
                    end = response.indexOf(endDeclareDate, start);
            String date = response.substring( start, end + endDeclareDate.length()   );
            String dateValue = response.substring( start + declareDate.length(), end );
            response = response.replace(date, dateValue);
        }
        return response;
    }
}
