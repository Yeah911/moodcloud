package com.moodcloud.moodcloud.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.Mood;

/**
 * Created by 626 on 22/06/2016.
 */
public class BlankFragment extends Fragment{

    private Mood mood;

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if( bundle != null ) {
            mood = new Mood();
            mood.setGuid( bundle.getString("id") );
            mood.setComment( bundle.getString("comment") );
            mood.setMark( bundle.getFloat("mark") );
            mood.setState( bundle.getString("state") );
//            mood.setDate( Utils.getDate(bundle.getString("date")) );
            mood.setType(Mood.MoodType.valueOf( bundle.getString("type") ) );
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_test_tab, container, false);

        TextView moodTV = (TextView) rootView.findViewById(R.id.moodRate);
        moodTV.setText(mood.getComment());
        moodTV.setMovementMethod(new ScrollingMovementMethod());
        TextView label = (TextView) rootView.findViewById(R.id.section_label);
        label.setText( "Note : " + mood.getMark() );

        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);

        MyAdapterCustom adapter = new MyAdapterCustom(getActivity(), mood.getGuid());
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return rootView;
    }

}
