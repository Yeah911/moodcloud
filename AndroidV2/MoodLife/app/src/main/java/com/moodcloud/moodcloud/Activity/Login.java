package com.moodcloud.moodcloud.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.applications.PubnubBroadcast;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.Request.HistoryRequests;
import com.moodcloud.moodcloud.persistance.Request.UserRequests;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.util.Utils;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private UserRequests usrRequest;
    private User usr;
    AsyncResponse ar;
    HistoryRequests hr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.loadingPanel).setVisibility(View.GONE);

        hr = new HistoryRequests(getApplicationContext());
        if ( hr.getLastIsAnAutoLog() && Informations.user != null ) { // auto log check box (chackbox not added)
            Intent i = new Intent(Login.this, Chat.class);
            startActivity(i);
        }
        ar = new AsyncResponse();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        usrRequest = new UserRequests( getApplicationContext() );
        final EditText login = (EditText) findViewById(R.id.loginName);
        final EditText password = (EditText) findViewById(R.id.password);

        Button connect = (Button) findViewById(R.id.button2);
        Button inscription = (Button) findViewById(R.id.signinButton);
        Button forgotPwd = (Button) findViewById(R.id.forgetButton);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String pwdHashed = Utils.encryptMD5(password.getText().toString(),false);
                sendRequestconnect(login.getText().toString(),pwdHashed);

            }

        });

        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, SignIn.class);
                startActivity(i);
            }
        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, ForgotPassword.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }

    public void sendRequestconnect(final String login, final String pwd){

        final CheckBox remember = (CheckBox) findViewById(R.id.checkBox);

        ar.sendRequestGet(Connection.URLRedirection.challenge, null, new CallbackResponse() {
            @Override
            public void onSuccess(String challenge) {
                Map<String, String> rp = new HashMap<>();
                String hash = Utils.encryptMD5( challenge + pwd);
                rp.put("userId", login);
                rp.put("secret", hash);
                ar.sendRequestGet(Connection.URLRedirection.authenticate, rp, null, new CallbackResponse() {

                    @Override
                    public void onSuccess(boolean connected) {
                        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                        if(connected) {
                            final EditText password = (EditText) findViewById(R.id.password);
                            usr = usrRequest.getUser(login, password.getText().toString());
                            if( usr == null ) {
                                usrRequest.register( new User(login, password.getText().toString(), login) );
                                usr = usrRequest.getUser(login, password.getText().toString());
                            }
                            Informations.user = usr;
                            hr.add(usr, "login", remember.isChecked());
                            PubnubBroadcast.subscribePrivateChannel(Login.this, new Intent(Login.this, Chat.class));
                            finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "error login/password", Toast.LENGTH_LONG).show();
                        }

                    }
                });

            }
        });
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);

    }

}
