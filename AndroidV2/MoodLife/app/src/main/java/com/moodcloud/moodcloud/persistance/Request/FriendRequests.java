package com.moodcloud.moodcloud.persistance.Request;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.moodcloud.moodcloud.model.FriendRelation;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.persistance.DAOAccess;
import com.moodcloud.moodcloud.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class FriendRequests {
    
    public FriendRequests(Context context) {
        access = new DAOAccess(context, name_table, SqlRequest.create_mood_friends);
        ur = new UserRequests(context);
    }
    UserRequests ur;

    public void acceptFriend(User asker, User user) {
        FriendRelation relation = getFriendRelation(asker, user);
        relation.setFriend(true);
        modifyRelation( relation );
    }

    public void blockFriend(User asker, User user) {
        FriendRelation relation = getFriendRelation(asker, user);
        relation.setBlocked(true);
        modifyRelation( relation );
    }

    public void refuseFriend(User asker, User user) {
        FriendRelation relation = getFriendRelation(asker, user);
        removeRelation(relation);
    }

    public void modifyRelation(FriendRelation relation) {
        access.open();
        ContentValues cv = new ContentValues();
        cv.put("id_user_ask", relation.getIdAsker());
        cv.put("id_user", relation.getIdAsked());
        cv.put("friend", relation.isFriend());
        cv.put("blocked", relation.isBlocked());
        access.modify(name_table, "id = ?", new String[]{relation.getId()}, cv);
        access.close();
    }

    public void removeRelation(FriendRelation relation) {
        access.open();
        access.delete(name_table, relation.getId());
        access.close();
    }

    public FriendRelation getFriendRelation(User asker, User user) {
        access.open();
        FriendRelation relation = new FriendRelation();
        String idAsker = asker.getId(),
                idAsked = user.getId();
        Cursor cursor = access.get(SqlRequest.select_all_asked_friends, new String[] {idAsker, idAsker, idAsked, idAsked});
        if (! cursor.moveToNext() ) {
            return null;
        }
        relation.setId( cursor.getString(0) );
        relation.setIdAsker(cursor.getString(1));
        relation.setIdAsked(cursor.getString(2));
        relation.setFriend(cursor.getInt(3) > 0);
        relation.setBlocked(cursor.getInt(4) > 0);
        return relation;
    }

    public void register(User asker, User asked) {
        access.open();
        ContentValues cv = new ContentValues();
        cv.put("id_user_ask", asker.getId());
        cv.put("id_user", asked.getId());
        cv.put("date", Utils.getCurrentDate());
        cv.put("friend", false);
        cv.put("blocked", false);
        access.add(name_table, cv);
        access.close();
    }

    public List<User> getFriends(User usr) {
        List<User> friends = new ArrayList<>();
        String id = usr.getId();
        access.open();
        Cursor cursor = access.get( SqlRequest.select_all_friends, new String[] {id, id} );
        while( cursor.moveToNext() ) {
            User user;
            String id_user_ask = cursor.getString(1);
            String id_user = cursor.getString(2);
            if ( id.equals(id_user_ask) ) {
                user = ur.getUser(id_user);
            } else {
                user = ur.getUser(id_user_ask);
            }
            friends.add(user);
        }
        return friends;
    }

    public List<User> getFriendsAsked(User usr) {
        List<User> friends = new ArrayList<>();
        String id = usr.getId();
        access.open();
        Cursor cursor = access.get( SqlRequest.select_all_friends_asked, new String[] {id} );
        while( cursor.moveToNext() ) {
            User user = new User();
            usr.setId(cursor.getString(0));
            usr.setLogin(cursor.getString(1));
            usr.setEmail(cursor.getString(2));
            friends.add(user);
        }
        return friends;
    }

    private DAOAccess access;
    private final String name_table = "friend";
}
