package com.moodcloud.moodcloud.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.moodcloud.moodcloud.Activity.Actuality.ContactInfo;
import com.moodcloud.moodcloud.R;
import com.moodcloud.moodcloud.model.Message;
import com.moodcloud.moodcloud.model.Mood;
import com.moodcloud.moodcloud.remote.AsyncResponse;
import com.moodcloud.moodcloud.remote.CallbackResponse;
import com.moodcloud.moodcloud.remote.Connection;
import com.moodcloud.moodcloud.remote.Parser;
import com.moodcloud.moodcloud.util.MyAdapter;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Chat extends AppCompatActivity{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private List<Mood> moods = new ArrayList<>();

    private int totalConversation;

    private TabLayout tabLayout;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.container);
//temporaire
        totalConversation = 3;
//        setTabsNumber();
        AsyncResponse.sendRequestGet(Connection.URLRedirection.retreiveUserMoods, new CallbackResponse(this) {
            @Override
            public void onSuccess(JSONArray json) {
                moods = Parser.parseMood(json);
                Collections.sort(moods, getComparator());
                totalConversation = 0;
                for( Mood mood : moods ){
                    if(!mood.getType().equals(Mood.MoodType.privateMood)) {
                        totalConversation++;
                    }
                }

                if( totalConversation > 0 )
                    setTabsNumber();
                else
                    Utils.toast(context, "aucun mood public ou protected");
            }

            @Override
            public void onSuccess() {
                Utils.toast(context, "aucun mood partagé");
            }
        });


    }

    private Comparator<Mood> getComparator() {
        return new Comparator<Mood>() {
            @Override
            public int compare(Mood lhs, Mood rhs) {

                return lhs.getDateLong()  < rhs.getDateLong() ? 1 :
                        lhs.getDateLong() > rhs.getDateLong() ? -1 : 0 ;
            }
        };
    }

    private void setTabsNumber() {

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),Chat.this);

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // request number mood where conversations
        tabLayout.setupWithViewPager(mViewPager);

        for (int i =0; i< totalConversation; i++) {
            int val = i+1;
            //tabLayout.addTab(tabLayout.newTab());
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(mSectionsPagerAdapter.getTabView(i));

        }

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        Context context;

        public SectionsPagerAdapter(FragmentManager fm, Context ctx) {
            super(fm);
            this.context = ctx;
        }

        private Fragment getFragment( int position, String moodId){
            Fragment fragment = new BlankFragment();
            Bundle args = new Bundle();
            args.putString("id" , moodId);
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new BlankFragment();
            Bundle args = new Bundle();
            Mood m = moods.get(position);
            args.putString("id" , m.getGuid());
            args.putString("comment" , m.getComment());
            args.putString("state" , m.getState());
            args.putString("date" , m.getDate().toString());
            args.putFloat ("mark" , m.getMark());
            args.putString("type" , m.getType().toString());

            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount() {
            return totalConversation;
        }

        @Override
        public String getPageTitle(int position) {
            String result;
            switch (position) {
                case 0:
                    result = "Mood n° 1";
                break;
                default:
                    result = "Mood n° " + String.valueOf(position+1);
                break;
            }
            return result;
        }

        public View getTabView(int position) {
            View tab = LayoutInflater.from(Chat.this).inflate(R.layout.custom_tab, null);
            TextView tv = (TextView) tab.findViewById(R.id.custom_text);
            tv.setText(this.getPageTitle(position));
            return tab;
        }
    }

    public void startConversation(Message message){
        Intent intent = new Intent(this,ChatView.class);
        intent.putExtra("message", message);
        intent.putExtra("origin",false);
        startActivity(intent);
    }
}
