package com.moodcloud.moodcloud.Activity.Actuality;

import com.moodcloud.moodcloud.model.Mood;
import com.moodcloud.moodcloud.model.User;

/**
 * Created by Emerich on 28/06/2016.
 */
public class ContactInfo {
    protected String name;
    protected String note;
    protected String com;
    protected User user;
    protected Mood mood;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCom() {
        return com;
    }

    public void setCom(String com) {
        this.com = com;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Mood getMood() {
        return mood;
    }

    public void setMood(Mood mood) {
        this.mood = mood;
    }

    protected static final String NAME_PREFIX = "Name : ";
    protected static final String NOTE_PREFIX = "Note :";
    protected static final String COM_PREFIX = "Commentaire :";
}
