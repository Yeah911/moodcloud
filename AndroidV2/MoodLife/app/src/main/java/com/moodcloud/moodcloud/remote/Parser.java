package com.moodcloud.moodcloud.remote;

import com.moodcloud.moodcloud.Activity.Actuality.ContactInfo;
import com.moodcloud.moodcloud.Activity.ChatMessage;
import com.moodcloud.moodcloud.applications.Informations;
import com.moodcloud.moodcloud.model.Conversation;
import com.moodcloud.moodcloud.model.ListModel;
import com.moodcloud.moodcloud.model.Message;
import com.moodcloud.moodcloud.model.Mood;
import com.moodcloud.moodcloud.model.MoodMessage;
import com.moodcloud.moodcloud.model.User;
import com.moodcloud.moodcloud.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Parser {


    /*
    *
    * {
    * BroadcastList : [
    * {id : "firstId", name : "firstName"},
    * {id : "seondId", name : "secondName"}
    * ]
    * }
    *
    * */
    public static void parseBroadcastList( String data ) {
        try {
            JSONObject object = new JSONObject(data);
            JSONArray array = object.getJSONArray("BroadcastList");
            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i);

                String id = o.getString("id");
                String name = o.getString("name");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     *  {
     *  FriendshipRequest : [
     *  {id : "id", state : ""}
     *  ]
     *  }
     *
     *  */
    public static void parseFriendshipState( String data ) {
        try {
            JSONObject object = new JSONObject(data);
            JSONArray array = object.getJSONArray("FriendshipRequest");
            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i);

                String id = o.getString("id");
                FriendshipState state = FriendshipState.valueOf(o.getString("state"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
    *
     *  {
     *  Mood : [
     *  {id : "id", rate : "", comment : "", moodType : "", creationDateTime : "", state : "", localization : ""}
     *  ]
     *  }
     *
    * */
    public static List<Mood> parseMood( JSONArray array ) {
        List<Mood> moods = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i).getJSONObject("mood");

                String id = o.getString("Id");
                float rate = (float) o.getDouble("Rate");
                String coment = o.getString("Comment");
                String typeMood = o.getString("MoodType").toLowerCase();
                Mood.MoodType type;
                if( typeMood.equals("privatemood") ) {
                    type = Mood.MoodType.privateMood;
                } else if( typeMood.equals("publicmood") ) {
                    type = Mood.MoodType.publicMood;
                } else {
                    type = Mood.MoodType.protectedMood;
                }
                long dateLong = o.getLong("CreationDatetime");
                Date creation = Utils.getDate( dateLong );
                MoodState state = MoodState.valueOf( o.getString("State") );
                moods.add( new Mood(id, rate, coment, type, state.toString(), creation, dateLong) );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return moods;
    }


    /*
    *
     *  {
     *  Conversation : [
     *  {id : "id", MessageCount : "", ExpireDate : "", lastUpdateDateTime : ""}
     *  ]
     *  }
     *
    * */
    public static List<Conversation> parseConversation( JSONObject json ) {
        List<Conversation> conversations = new ArrayList<>();
        try {
            JSONArray array = json.getJSONArray("conversations");
            List<Message> messages = new ArrayList<>();
            for (int i = 0; i < array.length(); i++){
                conversations.add( parseAConversation(array.getJSONObject(i)) );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return conversations;
    }

    private static Conversation parseAConversation(JSONObject json) throws JSONException {
        String id = json.getString("Id");
        int messageCount = json.getInt("MessageCount");
        Date expire = Utils.getDate( json.getLong("ExpiredDate") );
        Date lastUpdate = Utils.getDate( json.getLong("LastUpdateDatetime") );
        return new Conversation( id, messageCount, expire, lastUpdate );
    }

    public static User parseAnUser(JSONObject json) throws JSONException {
        String id = json.getString("Id");
        String mail = json.getString("Mail");
        String phoneNumber = json.getString("PhoneNumber");
        String name = json.getString("Name");
        return new User(id, name, mail, phoneNumber);
    }

    private static Mood parseAMood(JSONObject json) throws JSONException {
        Mood mood = new Mood();
        mood.setGuid( json.getString("Id") );
        mood.setComment( json.getString("Comment") );
        mood.setMark( (float) json.getDouble("Rate") );
        mood.setType(  json.getString("MoodType") );
        long dateLong = json.getLong("CreationDatetime");
        Date creation = Utils.getDate( dateLong );
        mood.setDate(creation);
        mood.setDateLong(dateLong);
        mood.setState( json.getString("State") );
        return mood;
    }

    public static Message parseMessagesConversation( JSONObject json ) {
        Message message = new Message();
        try {
            JSONObject jsonConv = json.getJSONObject("conversation");
            message.setConversation( parseAConversation(jsonConv) );
            JSONArray array = json.has("messages") ? json.getJSONArray("messages") : null;

            message.setFrom( parseAnUser(  json.getJSONObject("interlocutor") ) );
            message.setTo( parseAnUser(  json.getJSONObject("moodOwner") ) );
            message.setMood( parseAMood( json.getJSONObject("mood") ) );

            if( array.length() < 1 ) return message;
            for (int i = 0; i < array.length(); i++){
                JSONObject object = array.getJSONObject(i);
                JSONObject o = object.getJSONObject("message");
                long date = o.getLong("CreationDatetime");
                MoodMessage mm = new MoodMessage(
                        o.getString("Id"),
                        o.getString("Content"),
                        Utils.getDate( o.getLong("CreationDatetime") ),
                        date,
                        Parser.parseAnUser( object.getJSONObject("author") ) );

                message.addMessage( mm );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }


    /*
    *
     *  {
     *  Message : [
     *  {id : "id", Content : "", ExpireDate : "", CreationDateTime : ""}
     *  ]
     *  }
     *
    * */
    public static void parseMessage( String data ) {
        try {
            JSONObject object = new JSONObject(data);
            JSONArray array = object.getJSONArray("Message");
            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i);

                String id = o.getString("id");;
                String content = o.getString("Content");
                Date expire = Utils.getDate( o.getLong("ExpireDate") );
                Date creation = Utils.getDate( o.getLong("CreationDateTime") );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static List<User> parseUsers( JSONArray array ) {
        List<User> users = new ArrayList<>();
        try {

            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i);

                String id = o.getString("Id");
                String mail = o.getString("Mail");
                String phoneNumber = o.getString("PhoneNumber");
                String name = o.getString("Name");
                users.add( new User(id, name, mail, phoneNumber) );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static List<User> parseAskUsers( JSONArray array, List<String> idRequest) {
        List<User> users = new ArrayList<>();
        try {

            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i).getJSONObject("requester");
                JSONObject o2 = array.getJSONObject(i).getJSONObject("request");

                idRequest.add(o2.getString("Id"));

                String id = o.getString("Id");
                String mail = o.getString("Mail");
                String phoneNumber = o.getString("PhoneNumber");
                String name = o.getString("Name");
                users.add( new User(id, name, mail, phoneNumber) );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static List<ListModel> parseListFriends(JSONArray array ) {
        List<ListModel> listFriends = new ArrayList<>();
        List<User> users;
        ListModel model;
        try {
            for (int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i);
                String idList = o.getString("Id");
                String name = o.getString("Name");
                JSONArray members = o.getJSONArray("Members");
                users = new ArrayList<>();
                for( int j = 0; j < members.length(); j++ ) {
                    JSONObject membersList = members.getJSONObject(j);
                    String id = membersList.getString("Id");
                    String mail = membersList.getString("Mail");
                    String phoneNumber = membersList.getString("PhoneNumber");
                    String nameUser = membersList.getString("Name");
                    users.add(new User(id, nameUser, mail, phoneNumber));
                }
                model = new ListModel(idList, name, users);
                listFriends.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listFriends;
    }

    public static List<ContactInfo> parseMoodShared( JSONArray json ) {
        List<ContactInfo> contactInfos = new ArrayList<>();

        try {
            for( int i = 0; i < json.length(); i++ ) {
                JSONObject obj = json.getJSONObject(i);
                Mood m = parseAMood( obj.getJSONObject("mood") );
                User u = parseAnUser(obj.getJSONObject("publisher"));
                boolean show = false;
                ContactInfo ci = new ContactInfo();
                ci.setCom(m.getComment());
                ci.setMood(m);
                ci.setUser(u);
                ci.setName(u.getLogin());
                ci.setNote(m.getMark() + "");
                Mood.MoodType type = m.getType();
                switch (type) {
                    case publicMood:
                    case protectedMood:
                        show = true;
                        break;
                    case privateMood:
                        show = false;
                        break;
                }

                boolean ownMood = Informations.user.getEmail().equals(u.getEmail());

                if( show && !ownMood ) {
                    contactInfos.add(ci);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contactInfos;
    }

    public static ChatMessage parseChatMessage( JSONObject json ) {
        ChatMessage chat = new ChatMessage();
        try {
            JSONObject message = json.getJSONObject("message");
            chat.body = message.getString("Content");
            chat.date = Utils.getDateFromUnix(message.getString("CreationDatetime"));
            chat.dateLong = message.getLong("CreationDatetime");
            User u = parseAnUser(json.getJSONObject("author"));
            chat.sender = u.getLogin();
            chat.receiver = Informations.user.getLogin();
            Conversation conv = parseAConversation(json.getJSONObject("conversation"));
            chat.isMine = Informations.user.getEmail().equals(u.getEmail());
        } catch (JSONException e) {
            return null;
        }
        return chat;
    }

    public enum FriendshipState {
        created,
        submitted,
        accepted,
        refused
    };

    public enum NotificationState {
        seen,
        notSeen,
        skipped
    };

    public enum MoodState{
        Valid,
        expired
    };

    public enum UserState{
        valid,
        blocked,
        pending
    };
}